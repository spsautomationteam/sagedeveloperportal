package com.paymentcenter.testcases;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;

//import java.util.List;

//import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateAccount {

	public  WebDriver driver;

	public CreateAccount()
	{
		driver=Hooks.driver;
	}

	@Given("^Launch Sage Developer portal URL$")
	public void developer_Portal_is_opened() throws Throwable {

		UtilityFunctions.waitForPageToBeReady();

	}

	@When("^Click on 'Create Account' tab present at top left$")
	public void click_on_create_accoun_tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabCreateAccount", "");
		Thread.sleep(10000);

	}

	@Then("^Should display 'Create Account' screen$")
	public void create_account_screen_should_be() throws Throwable {

		UtilityFunctions.performValidation("pageTitle", "Create Account");

	}

	//	It should display fields to create new user account like Username, E-mail, First Name, Last Name, Company Name, Address 1, City, State, ZIP Code, Accept Terms & Conditions of Use, Create Account
	@Then("^Should display all require fields to create New User Account$")
	public void all_new_user_fields_should_be() throws Throwable 
	{
		UtilityFunctions.performValidation("labelFirstName", "First Name");
		UtilityFunctions.performValidation("labelLastName", "Last Name");
		UtilityFunctions.performValidation("labelUsername", "Username");
		UtilityFunctions.performValidation("labelEmailAddress", "E-mail address");
		//		UtilityFunctions.performValidation("labelCompanyName", "Company Name");
		UtilityFunctions.performValidation("labelCountry", "Country");
		UtilityFunctions.performValidation("labelAddress1", "Address 1");
		UtilityFunctions.performValidation("labelAddress2", "Address 2");
		UtilityFunctions.performValidation("labelCity", "City");
		UtilityFunctions.performValidation("labelState", "State");
		UtilityFunctions.performValidation("labelZIPCode", "ZIP code");
		UtilityFunctions.performValidation("labelTermsConditions", "Terms & Conditions");
		UtilityFunctions.performValidation("labelAcceptTermsConditionsofUse", "Accept Terms & Conditions of Use");
		UtilityFunctions.performValidation("btnCreateAccount", "Create Account");		

	}

	//	And Also it should display links like 'Sign in with facebook', 'Sign in with Google', 'Sign in with GitHub' option at the right side of the page
	@Then("^Also it should display 3 hyperlinks$")
	public void hyper_links_should_be() throws Throwable {

		UtilityFunctions.performValidation("linkFaceBook", "Sign in with Facebook");
		UtilityFunctions.performValidation("linkGoogle", "Sign in with Google");
		UtilityFunctions.performValidation("linkGitHub", "Sign in with GitHub");

	}

	@When("^Enter valid data in all required fields$")
	public void Enter_valid_data_in_all_fields(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performAction("fieldFirstName", data.get(1).get(1));
		UtilityFunctions.performValidation("fieldFirstName", data.get(1).get(1));

		UtilityFunctions.performAction("fieldLastName", data.get(2).get(1));
		UtilityFunctions.performValidation("fieldLastName", data.get(2).get(1));

		Random rand = new Random();
		int  n = rand.nextInt(500) + 1;
		String user = data.get(3).get(1)+n;
				
		UtilityFunctions.performAction("fieldUsername", user);
		UtilityFunctions.performValidation("fieldUsername", user);

		String val = data.get(4).get(1);		
		int x = val.indexOf("@");
		String y = val.substring(0, x);		
		String email = (y+n)+val.substring(x);	
		
		//UtilityFunctions.performAction("fieldEmailAddress", data.get(4).get(1));
		UtilityFunctions.performAction("fieldEmailAddress", email);
		UtilityFunctions.performValidation("fieldEmailAddress", email);

		UtilityFunctions.performAction("fieldCompanyName", data.get(5).get(1));
		UtilityFunctions.performValidation("fieldCompanyName", data.get(5).get(1));		

		UtilityFunctions.performAction("fieldCountry", data.get(6).get(1));

		Thread.sleep(3000);

		UtilityFunctions.performValidation("fieldCountry", data.get(6).get(1));

		Thread.sleep(3000);

		UtilityFunctions.performAction("fieldAddress1", data.get(7).get(1));
		UtilityFunctions.performValidation("fieldAddress1", data.get(7).get(1));

		UtilityFunctions.performAction("fieldAddress2", data.get(8).get(1));
		UtilityFunctions.performValidation("fieldAddress2", data.get(8).get(1));

		UtilityFunctions.performAction("fieldCity", data.get(9).get(1));
		UtilityFunctions.performValidation("fieldCity", data.get(9).get(1));

		UtilityFunctions.performAction("fieldState", data.get(10).get(1));
		UtilityFunctions.performValidation("fieldState", data.get(10).get(1));

		UtilityFunctions.performAction("fieldZIPCode", data.get(11).get(1));
		UtilityFunctions.performValidation("fieldZIPCode", data.get(11).get(1));

		//WebElement we = UtilityFunctions.retunWebElement("fieldAcceptTermsConditionsofUse");
		//WebElement link = UtilityFunctions.retunWebElement(linkName);		

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");

		UtilityFunctions.performAction("fieldAcceptTermsConditionsofUse", "");
		UtilityFunctions.performValidation("fieldAcceptTermsConditionsofUse", "true");

	}

	@Then("^Click on Create Account button$")
	public void click_on_create_account_button() throws Throwable {
		//		Thread.sleep(5000);
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0,250)", "");

		UtilityFunctions.performAction("btnCreateAccount", "");
		Thread.sleep(10000);

	}

	//	Welcome Message should be display like "A welcome message with further instructions has been sent to your e-mail address."
	@Then("^It should create new account and should display Welcome message along with Sign In button$")
	public void welcome_msg_should_be() throws Throwable 
	{
			UtilityFunctions.performValidation("msgWelcome", "");
			UtilityFunctions.performValidation("buttonSignIn", "Sign In");		

	}

	//	@When("^Enter wrong data in required fields$")
	//	public void enterInvalidData(DataTable invalidData) throws Throwable 
	//	{		
	//		List<List<String>> data = invalidData.raw();
	//		WebElement toClear = UtilityFunctions.retunWebElement(data.get(0).get(0));
	//		toClear.sendKeys(Keys.CONTROL + "a");
	//		toClear.sendKeys(Keys.DELETE);
	//		Thread.sleep(3000);
	//		UtilityFunctions.performAction(data.get(0).get(0),data.get(0).get(1));
	//		UtilityFunctions.retunWebElement(data.get(0).get(0)).sendKeys(Keys.TAB);
	//	
	//	} 

	@When("^Keep all Mandatory fields are in Blank$")
	public void keep_all_Mandatory_fields_are_in_Blank(DataTable table) throws Throwable {

//		List<List<String>> data = table.raw();	
//		
//		UtilityFunctions.performAction("fieldFirstName", data.get(1).get(1));
//		UtilityFunctions.performAction("fieldLastName", data.get(2).get(1));		
//		UtilityFunctions.performAction("fieldUsername", data.get(3).get(1));
//		
//		WebElement weEmail = UtilityFunctions.retunWebElement("fieldEmailAddress");		
//				((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weEmail);
//		
//		UtilityFunctions.performAction("fieldEmailAddress", data.get(4).get(1));
//		UtilityFunctions.performAction("fieldCompanyName", data.get(5).get(1));
//
//		Thread.sleep(3000);
//
//		UtilityFunctions.performAction("fieldAddress1", data.get(6).get(1));
//		UtilityFunctions.performAction("fieldCity", data.get(7).get(1));
//		UtilityFunctions.performAction("fieldState", data.get(8).get(1));
//		UtilityFunctions.performAction("fieldZIPCode", data.get(9).get(1));
		
		WebElement weAcceptTerms = UtilityFunctions.retunWebElement("fieldAcceptTermsConditionsofUse");		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weAcceptTerms);

	}

	//	Error_message_body_should_contain_for_all Mandatory Blank fields
	@Then("^error message body should contain \"(.*?)\"$")
	public void error_message_body_should_contain_for_Blankfields(String errorMsg) throws Throwable 
	{

		UtilityFunctions.performValidation("errMsgBlankFields", errorMsg);
		//		String errorMsgBode = driver.findElement(By.xpath(".//*[@id='messages']/div/ul")).getText();
		//		if(errorMsgBode.contains(errorMsg))		{
		//			Assert.assertTrue(errorMsgBode.contains(errorMsg));  
		//		}
		//		else 		{
		//			Assert.assertTrue(errorMsgBode.contains(errorMsg));  
		//		}

	}
}

