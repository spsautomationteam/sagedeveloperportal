package com.paymentcenter.testcases;

import java.net.HttpURLConnection;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Utility.UtilityFunctions;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SageDevMainPage {

	public  WebDriver driver;

	public SageDevMainPage()
	{
		driver=Hooks.driver;
	}

	//@When("^Click on (.*) tab present at top the Page$")
	@When("^Click on '(.*)' Tab present at top of the Page$")
	public void click_on_All_Tabs_Top_Of_Page(String tabName) throws Throwable {

		WebElement link = UtilityFunctions.retunWebElement(tabName);		
		if (link != null) {
			String linkUrl = link.getAttribute("href");
			if (linkUrl != null && !linkUrl.contains("javascript")) {
				try 
				{
					URL url = new URL(linkUrl);

					HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();

					httpURLConnect.setConnectTimeout(3000);

					httpURLConnect.connect();

					if(httpURLConnect.getResponseCode()==200)
					{
						System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
					}
					if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
					{
						System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
					}
				} catch (Exception e) {

				}
			}
		} else {
			//System.out.println("This is Broken link "+ linkUrl);
		}
		//			Click on link
		link.click();
		Thread.sleep(3000);							
	}

	//@When("^Click on (.*) tab present at top the Page$")
	@When("^Click on '(.*)' link present at middle of the Page")
	public void click_on_All_Links_Middle_Of_Page(String linkName) throws Throwable {	

		WebElement link = UtilityFunctions.retunWebElement(linkName);	
		
		//WebElement element = driver.findElement(By.id("id_of_element"));
		//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", link);
		JavascriptExecutor js = (JavascriptExecutor)driver;
//		js.executeScript("arguments[0].click()", link);
		Thread.sleep(500); 
		
		if (link != null) {
			String
			linkUrl = link.getAttribute("href");
			System.out.println(linkUrl);
			//System.out.println(!linkUrl.contains("javascript"));
			if (linkUrl != null && !linkUrl.contains("javascript")) {
				try 
				{
					URL url = new URL(linkUrl);

					HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();

					httpURLConnect.setConnectTimeout(3000);

					httpURLConnect.connect();

					if(httpURLConnect.getResponseCode()==200)
					{
						System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
					}
					if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
					{
						System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
					}
				} catch (Exception e) {

				}
			}
		} else {
			//System.out.println("This is Broken link "+ linkUrl);
		}
		//			Click on link
		//link.click();
		js.executeScript("arguments[0].click()", link);
		Thread.sleep(3000);
		
	}

	@Then("^Verify application should navigate to '(.*)' page properly$")
	public void Verify_Application_should_navigateTo_Corresponding_Page(String pageTitle) throws Throwable {

		UtilityFunctions.performValidation("titleAllPages", pageTitle);
		//		UtilityFunctions.performAction("tabHome", "");
		driver.navigate().back();
		Thread.sleep(3000);

	}

}