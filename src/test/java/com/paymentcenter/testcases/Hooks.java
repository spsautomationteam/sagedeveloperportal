package com.paymentcenter.testcases;

import java.io.File;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.Properties;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import Utility.UtilityFunctions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks
{
	public static WebDriver driver;
	public static Properties proprty;
	public String path;
	public FileInputStream fis = null;
	public FileOutputStream fileOut = null;

	//*********************************************************************
	//Function to read config file
	//*********************************************************************
	public static void filereadConfig() throws IOException
	{

		File src =new File("./ConfigReader/Config.properties");
		FileInputStream FIS = new FileInputStream(src);
		proprty = new Properties();
		proprty.load(FIS);

	}



	@Before
	//*********************************************************************
	//Function to open Web browser
	//*********************************************************************

	public static void openBrowser() throws Exception
	{
		filereadConfig();
		//Read config file which has object propertie

		String browserName = Hooks.proprty.getProperty("Browser");
		//FirefoxDriverPath

		if(browserName.equalsIgnoreCase("firefox"))
		{
			DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
			firefoxCapabilities.setCapability("nativeEvents", false);
			System.setProperty("webdriver.gecko.driver",Hooks.proprty.getProperty("FirefoxDriverPath"));
					//Hooks.proprty.getProperty("FirefoxDriverPath"));
			driver=new FirefoxDriver(firefoxCapabilities);
		}
		else if(browserName.equals("chrome"))
		{
			DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
			chromeCapabilities.setCapability("nativeEvents", false);	
			System.setProperty("webdriver.chrome.driver",Hooks.proprty.getProperty("ChromeDriverPath"));

			driver=new ChromeDriver(chromeCapabilities);
		}
		else if(browserName.equals("ie"))
		{
			System.setProperty("webdriver.ie.driver", 	Hooks.proprty.getProperty("IEDriverPath"));
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability("nativeEvents", true);

			driver = new InternetExplorerDriver(ieCapabilities);

		}

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
//		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(Hooks.proprty.getProperty("URL"));
		UtilityFunctions.waitForPageToBeReady();

	}

	/*
	 * Embed a screenshot in test report if test is marked as failed
	 */

	@After
	public void embedScreenshot(Scenario scenario) 
	{
		if(scenario.isFailed()) {
			try {
				//scenario.write("Current Page URL is " + Hooks.driver.getCurrentUrl());
				byte[] screenshot = ((TakesScreenshot)Hooks.driver).getScreenshotAs(OutputType.BYTES);
				scenario.embed(screenshot, "image/png");
				//System.out.println("scenario name after is : "+scenario.getName());
			} 
			catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots.getMessage());
			} 
		}
//		scenario.write("Test ended last");
//		scenario.write();
		Hooks.driver.quit();
	}
}


