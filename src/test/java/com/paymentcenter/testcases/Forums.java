package com.paymentcenter.testcases;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Forums {

	public  WebDriver driver;
	int heightBefCollage;
	int heightAftCollage;
	ArrayList<String> dataAsceding = new ArrayList<>();
	ArrayList<String> dataDesceding = new ArrayList<>();

	public Forums()
	{
		driver=Hooks.driver;
	}


	@When("^Click on 'Forums' tab in 'Sage Developer User' screen$")
	public void click_on_Forums_tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabForums", "");
		Thread.sleep(3000);
	}

	@Then("^Verify displaying Tabs at top of the Forums Page$")
	public void  Verify_Tabs_At_TopOf_Forum_Page() throws Throwable {

		UtilityFunctions.performValidation("tabApiSandBox", "");
		UtilityFunctions.performValidation("tabDocumentation", "");
		UtilityFunctions.performValidation("tabForums", "");
		UtilityFunctions.performValidation("tabSupport", "");
		//	UtilityFunctions.performValidation("tabCreateAccount", "");
		UtilityFunctions.performValidation("tabMyApps", "");
		UtilityFunctions.performValidation("tabLogout", "");
		UtilityFunctions.performValidation("tabHome", "");		

	}

	@Then("^Verify displaying main Tabs under Forums section$")
	public void Verify_Tabs_under_Forums_section() throws Throwable {

		UtilityFunctions.performValidation("tabViewForums", "");
		UtilityFunctions.performValidation("tabActiveTopics", "");
		UtilityFunctions.performValidation("tabUnAnsTopics", "");
		UtilityFunctions.performValidation("tabNewAndUptTopics", "");

	}

	@When("^Click on 'Forums' tab present at top left$")
	public void click_on_Forums_tab_Top_Left() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabForums", "");
		Thread.sleep(3000);
	}

	@Then("^Verify displaying Tabs at top of the Forums Page2$")
	public void  Verify_Tabs_At_TopOf_Forum_Page2() throws Throwable {

		UtilityFunctions.performValidation("tabApiSandBox", "");
		UtilityFunctions.performValidation("tabDocumentation", "");
		UtilityFunctions.performValidation("tabForums", "");
		UtilityFunctions.performValidation("tabSupport", "");
		UtilityFunctions.performValidation("tabCreateAccount", "");
		UtilityFunctions.performValidation("tabHome", "");		

	}

	@Then("^Verify displaying main Tabs under Forums section2$")
	public void Verify_Tabs_under_Forums_section2() throws Throwable {

		UtilityFunctions.performValidation("tabViewForums", "");
		UtilityFunctions.performValidation("tabActiveTopics", "");
		UtilityFunctions.performValidation("tabUnAnsTopics", "");

	}

	@When("^Click on 'View Forums' tab under Forums section$")
	public void click_on_ViewForums_Tab_under_Forums_Section() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabViewForums", "");
		Thread.sleep(3000);
	}

	@Then("^Verify It should display below list of the options under 'View Forums' tab$")
	public void Verify_Options_Under_ViewForums_Tab(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performValidation("linkGeneralDis", data.get(1).get(0));
		UtilityFunctions.performValidation("linkGettingStarted", data.get(2).get(0));
		UtilityFunctions.performValidation("linkDirectAPI", data.get(3).get(0));
		UtilityFunctions.performValidation("linkPaymentsJs", data.get(4).get(0));
		UtilityFunctions.performValidation("linkSageExcDesk", data.get(5).get(0));
		UtilityFunctions.performValidation("linkSageExcVirDesk", data.get(6).get(0));
		UtilityFunctions.performValidation("linkEMV", data.get(7).get(0));
		UtilityFunctions.performValidation("linkNewPosts", data.get(8).get(0));
		UtilityFunctions.performValidation("linkNoNewPosts", data.get(9).get(0));
		UtilityFunctions.performValidation("dropDownNameForumTools", data.get(10).get(0));		
		UtilityFunctions.performValidation("dropDownItem1", data.get(11).get(0));
		UtilityFunctions.performValidation("dropDownItem2", data.get(12).get(0));

	}

	@When("^Select '(.*)' option from Forum Tools drop down$")
	public void Select_Option_From_DropDown(String item) throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("dropDownNameForumTools", item);
		Thread.sleep(3000);
	}


	@Then("^It should switch to '(.*)' tab present in Forums page1$")
	public void Should_SwitchTo_ActiveForumTopics(String title) throws Throwable {

		UtilityFunctions.performValidation("titleActiveForumTopics", title);

	}

	@Then("^It should switch to '(.*)' tab present in Forums page2$")
	public void Should_SwitchTo_UnansweredForumTopics(String title) throws Throwable {

		UtilityFunctions.performValidation("titleUnansweredForumTopics", title);

	}

	@Then("^Verify Height of the Forum Table '(.*)' 'Collapsed'$")
	public void Verify_ForumTable_Hight_Before_Collapsed(String title) throws Throwable {

		WebElement Image = driver.findElement(By.xpath(Hooks.proprty.getProperty("sizeForumTable")));
		//		WebElement Image = driver.findElement(By.xpath(".//*[@class='forum-table forum-table-forums']"));
		heightBefCollage = Image.getSize().getHeight();        
		System.out.println("Before collapsed ForumTable Hight Is : "+heightBefCollage);

	}

	@When("^Click on collpase '-' icon present at top right corner of the Forums table$")
	public void Click_On_ForumTable_Collage_Icon() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("collpseIconForumTable", "");
		Thread.sleep(3000);				

	}

	@Then("^Forum Table should be 'Collapsed' properly$")
	public void ForumTable_should_be_Collapsed_properly() throws Throwable {

		WebElement Image = driver.findElement(By.xpath(Hooks.proprty.getProperty("sizeForumTable")));
		heightAftCollage = Image.getSize().getHeight();        
		System.out.println("After collapsed ForumTable Hight Is : "+heightAftCollage);

		if(heightBefCollage>heightAftCollage)
		{
			System.out.println("Forum Table 'Collapsed' properly");
		}
		else
		{
			System.out.println("Before Collapse Height : "+heightBefCollage+" After Collapsed Height : "+heightAftCollage);
			System.out.println("Forum Table Not 'Collapsed' properly");
		}
	}

	@When("^Click on 'General Discussion' link under 'View Forums' tab$")
	public void Click_On_GeneralDiscussion_Link() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("linkGeneralDis", "");
		Thread.sleep(3000);		


	}

	@Then("^Verify It should display below list of the options under 'General Discussion' screen$")
	public void Verify_Options_Under_GeneralDiscussion(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performValidation("sideHeddingGeneralDis", data.get(1).get(0));
		UtilityFunctions.performValidation("dropDownNameForumTools", data.get(2).get(0));
		UtilityFunctions.performValidation("linkNewPostsGD", data.get(3).get(0));
		UtilityFunctions.performValidation("linkNoNewPostsGD", data.get(4).get(0));
		UtilityFunctions.performValidation("linkHotTopicWithPosts", data.get(5).get(0));
		UtilityFunctions.performValidation("linkHotTopicWithoutNewPosts", data.get(6).get(0));
		UtilityFunctions.performValidation("linkStickyTopic", data.get(7).get(0));
		UtilityFunctions.performValidation("linkLockedTopic", data.get(8).get(0));
		UtilityFunctions.performValidation("dropDownLastPost", data.get(9).get(0));
		UtilityFunctions.performValidation("dropListDown", data.get(10).get(0));	
		UtilityFunctions.performValidation("buttonSort", data.get(11).get(0));	

	}

	@When("^Select '(.*)' option from drop down which is present below the table$")
	public void Select_Topic_Topic_Starter(String option) throws Throwable {
		Select dropdown = new Select(driver.findElement(By.xpath(".//*[@name='order']")));
		dropdown.selectByVisibleText(option);
		Thread.sleep(1000);
	}

	@When("^Select '(.*)' from the next combo selection$")
	public void Select_UP_From_Dropdown(String option) throws Throwable {
		Select dropdown = new Select(driver.findElement(By.xpath(".//*[@name='sort']")));
		dropdown.selectByVisibleText(option);
		Thread.sleep(1000);
	}

	@When("^Click on 'Sort' button beside the drop down$")
	public void click_on_Sort_button() throws Throwable {
		UtilityFunctions.performAction("buttonSort", "");
		Thread.sleep(3000);
	}

	@Then("^Data in the '(.*)' column should be 'Ascending Order' with xpath '(.*)'$")
	public void Data_Should_be_Ascending_Order(String text, String xpath) throws Throwable {

		System.out.println("Forums text :"+text +"  xpath "+xpath);

		ArrayList<WebElement> textReply = new ArrayList<>();
		List<WebElement> listReply = UtilityFunctions.retunWebElements(xpath);
		System.out.println("Total Records : "+listReply.size());
		for(int i=0;i<listReply.size();i++)
		{
			textReply.add(listReply.get(i));
		}

		System.out.println("Total Records : "+textReply.size());
		System.out.println("Asceding Oder Data");

		for(int j=0;j<textReply.size();j++)
		{
			System.out.println(j+". "+textReply.get(j).getText());
			dataAsceding.add(textReply.get(j).getText());
		}	

	}

	@Then("^Data in the '(.*)' column should be 'Descending Order' with xpath '(.*)'$")
	public void Data_Should_Descending_Order(String text, String xpath) throws Throwable {

		ArrayList<WebElement> textReply = new ArrayList<>();
		List<WebElement> listReply = UtilityFunctions.retunWebElements(xpath);
		System.out.println("Total Records : "+listReply.size());
		for(int i=0;i<listReply.size();i++)
		{
			textReply.add(listReply.get(i));
		}

		System.out.println("Total Records : "+textReply.size());
		System.out.println("Desceding Oder Data");

		for(int j=0;j<textReply.size();j++)
		{
			System.out.println(j+". "+textReply.get(j).getText());
			dataDesceding.add(textReply.get(j).getText());
		}

		System.out.println(dataAsceding.get(0).trim());
		System.out.println(dataDesceding.get(textReply.size()-1));

		if(dataAsceding.get(0).trim().equals(dataDesceding.get(textReply.size()-1).trim()))
		{
			System.out.println("General Discussion Data Sorted Properly under "+text+" Column");
			//Assert.assertNotEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());
			Assert.assertEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());
		}
		else
		{
			System.out.println("General Discussion Data Not Sorted Properly under "+text+" Column");
			//Assert.assertEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());
			Assert.assertNotEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());

		}

	}



	@Then("^Table Data in the '(.*)' column should be 'Ascending Order' with xpath '(.*)'$")
	public void Data_Should_be_Ascending_Order_Two(String text, String xpath) throws Throwable {

		System.out.println("Forums text :"+text +"  xpath "+xpath);

		ArrayList<WebElement> textReply = new ArrayList<>();
		List<WebElement> listReply = UtilityFunctions.retunWebElements(xpath);
		//		System.out.println("Total Records : "+listReply.size());
		for(int i=0;i<listReply.size();i++)
		{
			textReply.add(listReply.get(i));
		}

		System.out.println("Total Records : "+textReply.size());
		System.out.println("***************Asceding Oder Data****************");

		for(int j=0;j<textReply.size();j++)
		{
			String postDate = textReply.get(j).getText();
			System.out.println(j+". "+postDate);
			String[] postDate1 = postDate.split("\n");
			String[] postDate2 = postDate1[1].split(",");
			String[] postDate3 = postDate2[1].split("-");
			System.out.println("Date : "+postDate3[0]);
			dataAsceding.add(postDate3[0]);
		}

	}

	@Then("^Table Data in the '(.*)' column should be 'Descending Order' with xpath '(.*)'$")
	public void Data_Should_be_Descending_Order_Two(String text, String xpath) throws Throwable {

		ArrayList<WebElement> textReply = new ArrayList<>();
		List<WebElement> listReply = UtilityFunctions.retunWebElements(xpath);
		//		System.out.println("Total Records : "+listReply.size());
		for(int i=0;i<listReply.size();i++)
		{
			textReply.add(listReply.get(i));
		}

		System.out.println("Total Records : "+textReply.size());
		System.out.println("****************Desceding Oder Data*******************");

		for(int j=0;j<textReply.size();j++)
		{
			String postDate = textReply.get(j).getText();
			System.out.println(j+". "+postDate);
			String[] postDate1 = postDate.split("\n");
			String[] postDate2 = postDate1[1].split(",");
			String[] postDate3 = postDate2[1].split("-");
			System.out.println("Date : "+postDate3[0]);
			dataDesceding.add(postDate3[0]);
		}

		System.out.println(dataAsceding.get(0).trim());
		System.out.println(dataDesceding.get(textReply.size()-1));

		if(dataAsceding.get(0).trim().equals(dataDesceding.get(textReply.size()-1).trim()))
		{
			System.out.println("General Discussion Data Sorted Properly under "+text+" Column");
			Assert.assertEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());
		}
		else
		{
			System.out.println("General Discussion Data Not Sorted Properly under "+text+" Column");
			Assert.assertNotEquals(dataAsceding.get(0).trim(), dataDesceding.get(textReply.size()-1).trim());

		}

	}

	@When("^Click on 'NEW TOPIC' link displaying under 'General Discussion' header$")
	public void Click_On_NewTopic_Link() throws Throwable {
		UtilityFunctions.performAction("linkNewTopic", "");
		Thread.sleep(3000);

	}

	@Then("^Should display 'NEW TOPIC' screen along with require fields$")
	public void NewTopic_fields_should_be_Display() throws Throwable {

		UtilityFunctions.performValidation("fieldSubject", "");
		UtilityFunctions.performValidation("fieldForums", "General Discussion");
		UtilityFunctions.performValidation("fieldBodyTextArea", "");
		
		//((JavascriptExecutor)driver).executeScript("scroll(0,200)");
		
		UtilityFunctions.performValidation("linkSwithToPlainRichEditor", "");
		UtilityFunctions.performValidation("textFormatDropDown", "Full HTML");
		
		//((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		
		UtilityFunctions.performValidation("buttonSave", "Save");
		UtilityFunctions.performValidation("buttonPreview", "Preview");

	}

	@When("^Keep 'Subject' Mandatory field as Blank and Click on Save button$")	
	public void Keep_Blank_Click_On_Save_Button() throws Throwable {

		//JavascriptExecutor jse = (JavascriptExecutor)driver;
		//jse.executeScript("scroll(0, 250)"); // if the element is on bottom.

		WebElement weSave = UtilityFunctions.retunWebElement("buttonSave");		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weSave);

		UtilityFunctions.performAction("buttonSave", "");
		Thread.sleep(3000);

	}

	@Then("^Error message should be display$")
	public void Error_Message_Should_be_Display() throws Throwable {
		UtilityFunctions.performValidation("errorMSg", "Subject field is required.");
		Thread.sleep(1000);

	}

	@When("^Enter valid data into all fields in 'NEW TOPIC' page$")	
	public void Enter_ValidData_AllFields_In_NewTopics_Page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performAction("fieldSubject", data.get(1).get(1));
		UtilityFunctions.performAction("fieldForums", data.get(2).get(1));
		
		WebElement weSwitchPlainText = UtilityFunctions.retunWebElement("linkSwithToPlainRichEditor");		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weSwitchPlainText);
		
		UtilityFunctions.performAction("linkSwithToPlainRichEditor", "");
		UtilityFunctions.performAction("fieldBodyPlainText", data.get(3).get(1));

	}

	@When("^Click on 'Preview' button$")	
	public void Click_On_Preview_Button() throws Throwable {
		
		WebElement wePreview = UtilityFunctions.retunWebElement("buttonPreview");		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", wePreview);
		
		UtilityFunctions.performAction("buttonPreview", "");
		Thread.sleep(3000);

	}

	@When("^Click on 'Save' button$")	
	public void Click_On_Save_Button() throws Throwable 
	{
		WebElement weSave = UtilityFunctions.retunWebElement("buttonSave");		
		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weSave);

		UtilityFunctions.performAction("buttonSave", "");
		Thread.sleep(3000);

	}

	@Then("^The data we entered should be displayed in Preview Trimmed version Page$")	
	public void Entered_Data_Should_Display_Preview_Page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();		

		UtilityFunctions.performValidation("textPreviewSubject", data.get(1).get(1));
		UtilityFunctions.performValidation("textDropDownGD", data.get(2).get(1));
		UtilityFunctions.performValidation("textPreviewBody", data.get(3).get(1));

		UtilityFunctions.performValidation("textSubjectTwo", data.get(1).get(1));
		UtilityFunctions.performValidation("textBodyTwo", data.get(3).get(1));


	}

	@Then("^The data we entered should be displayed in 'Subject has been Created' page$")	
	public void Entered_Data_Should_Display_Subject_Created_Page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();		

		UtilityFunctions.performValidation("successMSg", "");
		UtilityFunctions.performValidation("textSaveSubject", data.get(1).get(1));
		UtilityFunctions.performValidation("textSaveBody", data.get(3).get(1));		

		//		Verify Submitted data at Navigation Tabs
		UtilityFunctions.performValidation("textAtForumsTab", data.get(2).get(1));
		UtilityFunctions.performValidation("textAtSubjectTab", data.get(1).get(1));

	}

	@When("^Click on 'General Discussion' Navigation tab at Subject has been created page$")	
	public void Click_On_GeneralDiscussion_Navigation_Tab() throws Throwable {
		UtilityFunctions.performAction("textAtForumsTab", "");
		Thread.sleep(3000);

	}

	@Then("^The added Subject text should be listed under 'General Discussion' table$")	
	public void Subject_Text_ShouldBe_Listed_Under_GDTable(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();		

		//		Verify Submitted data at Table
		UtilityFunctions.performValidation("textAtTopicStarter", data.get(1).get(1));

	}

	@Then("^Click on All Forum Tabs and Create New Post and Validate the Submitted data in All required locations$")	
	public void Click_All_ForumTabs_Submit_And_Validate_NewPost_Data(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();		

		String[] xpathAllTabs = {"linkGeneralDis","linkGettingStarted","linkDirectAPI","linkPaymentsJs","linkSageExcDesk","linkSageExcVirDesk","linkEMV"};
		int z= 100;
		for(int k=0; k<data.size()-1;k++)
		{
			
				//WebElement weAllTabs = UtilityFunctions.retunWebElement(xpathAllTabs[k]);		
				//((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weAllTabs);
				
				((JavascriptExecutor)driver).executeScript("window.scrollBy(100,"+z+")");
			
			UtilityFunctions.performAction(xpathAllTabs[k], "");
			Thread.sleep(3000);
			UtilityFunctions.performAction("linkNewTopic", "");
			Thread.sleep(3000);

			//		Enter the data into all New Topic fields
			UtilityFunctions.performAction("fieldSubject", data.get(k+1).get(1));
			UtilityFunctions.performAction("fieldForums", data.get(k+1).get(0));

			WebElement weSwithToPlainRichEditor = UtilityFunctions.retunWebElement("linkSwithToPlainRichEditor");		
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weSwithToPlainRichEditor);

			UtilityFunctions.performAction("linkSwithToPlainRichEditor", "");
			UtilityFunctions.performAction("fieldBodyPlainText", data.get(k+1).get(2));

			WebElement weSave = UtilityFunctions.retunWebElement("buttonSave");		
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView()", weSave);

			UtilityFunctions.performAction("buttonSave", "");
			Thread.sleep(3000);

			//		Verify displaying text at 'Submitted data displaying' page
			UtilityFunctions.performValidation("headerAtSubmitPage", data.get(k+1).get(1));
			UtilityFunctions.performValidation("successMSg", "");
			UtilityFunctions.performValidation("textSaveSubject", data.get(k+1).get(1));
			UtilityFunctions.performValidation("textSaveBody", data.get(k+1).get(2));		

			//				Verify Submitted data at Navigation Tabs
			UtilityFunctions.performValidation("textAtForumsTab", data.get(k+1).get(0));
			UtilityFunctions.performValidation("textAtSubjectTab", data.get(k+1).get(1));

			//		Click on Forum navigation tab i.e General Disuccsion
			UtilityFunctions.performAction("textAtForumsTab", "");
			Thread.sleep(3000);

			//		Verify text at Forum tab header (Ex : General Disuccsion)
			UtilityFunctions.performValidation("ForumTabHeader", data.get(k+1).get(0));

			//		Verify Subject text under Forum tab(Ex: General Disuccsion) table (Ex : Test Subject)
			UtilityFunctions.performValidation("textSubjectAtTopicStarter", data.get(k+1).get(1));

			UtilityFunctions.performAction("tabForums", "");
			Thread.sleep(3000);
			
			z = z+100;;

		}
	}

	@When("^Click on 'Active Topics' tab under Forums section$")
	public void Click_On_ActiveTopics_Tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabActiveTopics", "");
		Thread.sleep(3000);		

	}

	@Then("^Verify should display the required options under 'Active Topics' tab$")
	public void Verify_Options_Under_ActiveTopics_Tab() throws Throwable {

		UtilityFunctions.performValidation("ForumTabsDropDownAT", "- Any -");
		UtilityFunctions.performValidation("buttonApply", "Apply");
		UtilityFunctions.performValidation("tableAT", "");

	}

	@When("^Click on 'Unanswered topics' tab under Forums section$")
	public void Click_On_UnansweredTopics_Tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabUnAnsTopics", "");
		Thread.sleep(3000);		

	}

	@Then("^Verify It should display the required options under 'Unanswered topics' tab$")
	public void Verify_Options_Under_UnansweredTopics_Tab() throws Throwable {

		UtilityFunctions.performValidation("ForumTabsDropDownAT", "- Any -");
		UtilityFunctions.performValidation("buttonApply", "Apply");
		UtilityFunctions.performValidation("tableAT", "");

	}

	@Then("^Select All list Items and Verify Displaying data under '(.*)' table with xpath '(.*)'$")
	public void Select_All_ListItems_And_Verify_Data_Under_UnansweredTopics_Tab(String textTab, String xpath, DataTable table) throws Throwable {			

		List<List<String>> data = table.raw();		

		for(int k=0; k<data.size()-1;k++)
		{
			UtilityFunctions.performAction("ForumTabsDropDownAT", data.get(k+1).get(0));
			UtilityFunctions.performAction("buttonApply", "");
			Thread.sleep(2000);

			String status = driver.findElement(By.xpath(Hooks.proprty.getProperty("NoRecordsStatus"))).getText();
			if(status.trim().equalsIgnoreCase("No new or updated posts.") && textTab.equals("New & updated topics"))
			{
				System.out.println("No records for Forum : '"+data.get(k+1).get(0)+"' under 'New & updated topics' Tab");
			}	
			else if(status.trim().equalsIgnoreCase("No active topics.") && textTab.equals("Active Topics"))
			{
				System.out.println("No records for Forum : '"+data.get(k+1).get(0)+"' under 'Active Topics' Tab");
			}
			else if(status.trim().equalsIgnoreCase("No unanswered topics.") && textTab.equals("Unanswered Topics"))
			{
				System.out.println("No records for Forum : '"+data.get(k+1).get(0)+"' under 'Unanswered Topics' Tab");
			}

			else
			{
				ArrayList<WebElement> textReply = new ArrayList<>();
				List<WebElement> listReply = UtilityFunctions.retunWebElements(xpath);	

				for(int i=0;i<listReply.size();i++)
				{
					textReply.add(listReply.get(i));
				}

				System.out.println("Total Records : "+textReply.size());
				System.out.println("****************Verify Data for "+data.get(k+1).get(0).trim()+"*******************");

				for(int j=0;j<textReply.size();j++)
				{
					System.out.println(j+". "+textReply.get(j).getText());
					Assert.assertEquals(data.get(k+1).get(0).trim(), textReply.get(j).getText().trim());

				}
			}
		}

	}

	@When("^Click on 'New & updated topics' tab under Forums section$")
	public void Click_On_NewUpdatedTopics_Tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabNewAndUptTopics", "");
		Thread.sleep(3000);		

	}

	@Then("^Verify It should display the required options under 'New & updated topics' tab$")
	public void Verify_Options_Under_NewUpdatedTopics_Tab() throws Throwable {

		UtilityFunctions.performValidation("ForumTabsDropDownAT", "- Any -");
		UtilityFunctions.performValidation("buttonApply", "Apply");
		if(driver.findElement(By.xpath(Hooks.proprty.getProperty("NoRecordsStatus"))).isDisplayed())
			System.out.println("Topics Table Not Exist for Records");
		else
			UtilityFunctions.performValidation("tableAT", "");			

	}

	@When("^Click on 'Apply' button$")	
	public void Click_On_Apply_Button() throws Throwable {
		UtilityFunctions.performAction("buttonApply", "");
		Thread.sleep(3000);

	}


}

