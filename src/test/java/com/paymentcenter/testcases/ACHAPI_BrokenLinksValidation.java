package com.paymentcenter.testcases;
//package com.paymentcenter.testcases;
//
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//
//import Utility.UtilityFunctions;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class BrokenLinksValidation_ACHAPI {
//
//	public  WebDriver driver;
//	private int invalidLinksCount;
//
//	public BrokenLinksValidation_ACHAPI()
//	{
//		driver=Hooks.driver;
//	}
//
//
//	@When("^Click on 'API SandBox' tab present at top of the Page$")
//	public void click_on_Forums_tab() throws Throwable {
//		//		Thread.sleep(5000);
//		UtilityFunctions.performAction("tabPartners", "");
//		Thread.sleep(3000);
//	}
//
//	@Then("^Verify displaying Elements at Partners Page$")
//	public void  Verify_Tabs_At_TopOf_Forum_Page() throws Throwable {
//
//		//	UtilityFunctions.performValidation("xpathFeaturedPartner", "   Featured partner ");
//		UtilityFunctions.performValidation("pageHeaderPartners", "Sage Partners");
//		UtilityFunctions.performValidation("pageNavigationPartners", "Sage Partners");
//		UtilityFunctions.performValidation("listPartnersSection", "");		
//
//	}
//
//	@When("^Click on each Partner link and verify relavent Partner page display$")
//	public void click_on_All_Partners_links() throws Throwable {
//
//		try {
//
//			invalidLinksCount = 0;
//
//			String xpathlistPartners = ".//*[@id='partners-page-2']/div/div/div/a";
//			List<WebElement> listPartners = driver.findElements(By.xpath(xpathlistPartners));
//			System.out.println("Total Partners links : "+listPartners.size());
//			int i=1;
//			for(WebElement link : listPartners)
//			{
//				if (link != null) {
//					String url = link.getAttribute("href");
//					if (url != null && !url.contains("javascript")) {
////						verifyURLStatus(url);
//						verifyURLStatus(url);
//					} else {
//						invalidLinksCount++;
//					}
//
//					//			Click on Partner link
//					link.click();
//
//					Thread.sleep(3000);
//
//					ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
//					driver.switchTo().window(tabs2.get(1));
//					String partnerUrl = driver.getCurrentUrl();
//					
//					String title = driver.getTitle();
//					System.out.println(i+". Url of '"+title+"' = "+partnerUrl);
//					driver.close();
//					driver.switchTo().window(tabs2.get(0));
//				}
//
//				i++;			
//
//			}	
//
//			System.out.println("Total no. of invalid links are "
//					+ invalidLinksCount);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.println(e.getMessage());
//		}
//
//
//	}
//
//	public void verifyURLStatus(String linkUrl) {
//
//		 try 
//	        {
//	           URL url = new URL(linkUrl);
//	           
//	           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
//	           
//	           httpURLConnect.setConnectTimeout(3000);
//	           
//	           httpURLConnect.connect();
//	           
//	           if(httpURLConnect.getResponseCode()==200)
//	           {
//	               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
//	            }
//	          if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND)  
//	           {
//	               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
//	            }
//	        } catch (Exception e) {
//	           
//	        }
//	}
//}
//
//
