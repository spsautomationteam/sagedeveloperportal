package com.paymentcenter.testcases;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Company_Apps {


	public  WebDriver driver;
	String appName;
	String appDetails;
	String listOfProducts;
	String deleteAppName;
	String editAppName;
	String compName;
	String emailId;


	public Company_Apps()
	{
		driver=Hooks.driver;
	}	

	@When("^Click on 'Manage Companies/Invitations' link$")
	public void Click_On_ManageCompany_link() throws Throwable {

		UtilityFunctions.performAction("linkManageCompanies", "");
		Thread.sleep(3000);
	}

	@Then("^Verify It should display the required options under 'Manage Companies/Invitations' page$")
	public void Verify_Fields_Under_ManageCompanies_Page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("headerMangCompanies", data.get(1).get(0));
		UtilityFunctions.performValidation("textCompanies", data.get(1).get(0));
		UtilityFunctions.performValidation("buttonCreateCompany", data.get(2).get(0));
		UtilityFunctions.performValidation("textInvitations", data.get(3).get(0));		

	}


	//	@Then("^Verify It should display the required options under 'Add A New App' page$")
	//	public void Verify_Fields_Under_AddNewApp_Page(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performValidation("labelAppName", data.get(1).get(0));
	//		UtilityFunctions.performValidation("labelDetOfInt", data.get(2).get(0));
	//		UtilityFunctions.performValidation("checkboxSecIntSect", data.get(3).get(0));
	//		UtilityFunctions.performValidation("checkBoxSecProduct", data.get(4).get(0));
	//		UtilityFunctions.performValidation("buttonCreateApp", data.get(5).get(0));
	//		UtilityFunctions.performValidation("navigationTabAddApp", data.get(6).get(0));
	//
	//	}
	//	
	@When("^Click on 'CREATE COMPANY' button$")
	public void Click_On_CreateCompany_btton() throws Throwable {

		UtilityFunctions.performAction("buttonCreateCompany", "");
		Thread.sleep(3000);
	}

	@Then("^Verify It should display the required options under 'Create Company' page$")
	public void Verify_Fields_Under_CreatCompany_Page(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("headerCreateCompany", data.get(1).get(0));
		UtilityFunctions.performValidation("navigatioTabCreateCompany", data.get(1).get(0));
		UtilityFunctions.performValidation("labelCompanyName2", data.get(2).get(0));
		UtilityFunctions.performValidation("buttonSaveCompany", data.get(3).get(0));

	}

	@When("^Enter value into Company Name field$")
	public void Enter_value_into_CompanyName_field() throws Throwable {

		Random rand = new Random();
		int  n = rand.nextInt(500) + 1;
		compName = "Sage"+n;

		UtilityFunctions.performAction("fieldCompanyName2", compName);
		Thread.sleep(3000);
	}

	@When("^Click on 'Save Company' button$")
	public void Click_On_SaveCompany_btton() throws Throwable {

		UtilityFunctions.performAction("buttonSaveCompany", "");
		Thread.sleep(3000);
	}

	@Then("^Should display 'Success' msg and verify submitted data in 'Create company' page$")
	public void Should_Display_SuccessMsg_ValidData_CreateNewCompanyPage(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("headerCreateCompany", data.get(1).get(0));
		UtilityFunctions.performValidation("succMsgCompanyName", data.get(2).get(0));
		UtilityFunctions.performValidation("navigationTabMyApps", data.get(1).get(0));

	}

	@Then("^Verify created new company should listed under Company Apps and Manage Companies links$")
	public void NewCompany_Should_listed_At_CompanyApps_ManageCompanies() throws Throwable {

		ArrayList<WebElement> listCompany = new ArrayList<>();
		ArrayList<WebElement> listCompany2 = new ArrayList<>();

		ArrayList<String> text1 = new ArrayList<>();
		ArrayList<String> text2 = new ArrayList<>();

		List<WebElement> listOfCompanies_CompanyApps = UtilityFunctions.retunWebElements("listOfCompanies_CompanyApps");
		System.out.println("Total Companies at Company Apps : "+listOfCompanies_CompanyApps.size());

		List<WebElement> listOfCompanies_MangCompanies = UtilityFunctions.retunWebElements("listOfCompanies_MangCompanies");
		System.out.println("Total Companies at Manage Companies : "+listOfCompanies_MangCompanies.size());

		for(int i=0;i<listOfCompanies_CompanyApps.size();i++)
		{
			listCompany.add(listOfCompanies_CompanyApps.get(i));
			listCompany2.add(listOfCompanies_MangCompanies.get(i));

			text1.add(listCompany.get(i).getText());
			text2.add(listCompany2.get(i).getText());

		}				

		if((text1.contains(compName)) && (text2.contains(compName)))
		{
			System.out.println("Created company ("+compName+") listed under Company Apps and Manage Companies links");
		}
		else
		{
			System.out.println("Created company ("+compName+") Not listed under Company Apps and Manage Companies links");
		}
	}
	
	@When("^Keep Company Name field as Blank and Click on 'Save Company' button$")
	public void Click_On_SaveCompany_btton_By_BlankFields() throws Throwable {

		UtilityFunctions.performAction("buttonSaveCompany", "");
		Thread.sleep(3000);
	}
	
	@Then("^Should display 'Error' msgs for blank 'Company Name' and 'Internal Name' fields$")
	public void Should_Display_ErrMsg_ForBlankFields_AddNewAppPage(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		String[] errorMsgActSplit =UtilityFunctions.retunWebElement("errMsgBlankFieldsCreateCompany").getText().split("\n");

		for(int k=0; k<errorMsgActSplit.length; k++)
		{
			Assert.assertEquals(data.get(k).get(0).trim(),errorMsgActSplit[k].trim()); 
		}
	}
	
	@Then("^Select required Company from the Manage Companies list$")
	public void Select_required_Company_from_ManageCompanylist() throws Throwable {

		List<WebElement> listOfCompanies_MangCompanies = UtilityFunctions.retunWebElements("listOfCompanies_MangCompanies");
		System.out.println("Total Companies at Manage Companies : "+listOfCompanies_MangCompanies.size());

		System.out.println("Seleted company for sending the Invitation : "+listOfCompanies_MangCompanies.get(0).getText());		
			listOfCompanies_MangCompanies.get(0).click();
					
	}
	
	@When("^Enter Email Id into 'User Email' for sending Invitation$")
	public void Enter_EmailID_into_UserEmail_field() throws Throwable {

		emailId = "kumar.midhuna@gmail.com";
		UtilityFunctions.performAction("fieldUserEmailID", emailId);
		Thread.sleep(2000);
	}

	@When("^Click on 'Invite' button$")
	public void Click_On_Invite_btton() throws Throwable {

		UtilityFunctions.performAction("buttonInvite", "");
		Thread.sleep(3000);
	}
	

	@Then("^Verify Success for User Invitation and Other Data$")
	public void Verify_SuccessMsg_for_UserInvitation(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("succMsgDeveloperInvited", data.get(1).get(0));
		UtilityFunctions.performValidation("pendingInvitaion", data.get(2).get(0));
		UtilityFunctions.performValidation("buttonCancelInvitation", data.get(3).get(0));
		UtilityFunctions.performValidation("emailId", emailId);

	}
	
	@When("^Click on 'Cancel' button to cancel the 'Developer' Invitation$")
	public void Click_Cancel_button_To_cancel_DeveloperInvitation() throws Throwable {

		UtilityFunctions.performAction("buttonCancelInvitation", "");
		Thread.sleep(3000);
	}
	
	@Then("^Verify confirmation details for Cancel Invitation$")
	public void Verify_confirmation_Details_for_CancelInvitation(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();	

		String emailIDConfirm = data.get(1).get(0)+" "+emailId;
		UtilityFunctions.performValidation("ConfrmCancelInv", emailIDConfirm);
		UtilityFunctions.performValidation("buttonCancelInvita", data.get(2).get(0));
		UtilityFunctions.performValidation("buttonCancel", data.get(3).get(0));

	}
	
	@When("^Click on 'Cancel Invitaion' button$")
	public void Click_CancelInvitation_button() throws Throwable {

		UtilityFunctions.performAction("buttonCancelInvita", "");
		Thread.sleep(3000);
	}
	
	@Then("^Verify Success Msg for 'Cancel Invitation'$")
	public void Verify_SuccessMsg_for_CancelledInvitation() throws Throwable {
		
		String succMsgInvitaCancelled = "Request sent to developer "+emailId+" cancelled successfully.";
		
		UtilityFunctions.performValidation("succMsgInvitationCancelled", succMsgInvitaCancelled);

	}
	
	@When("^Keep 'User Email' field Blank and Click on 'Invite' button$")
	public void Keep_UserEmailID_Blank_And_Click_On_Invite_btton() throws Throwable {

		UtilityFunctions.performAction("buttonInvite", "");
		Thread.sleep(3000);
	}
	
	@Then("^Verify Error Msg for Blank 'User Email' field$")
	public void Verify_ErrorMsg_for_Blank_UserEmail_field(DataTable table) throws Throwable {
		
		List<List<String>> data = table.raw();	

		UtilityFunctions.performValidation("errorMsgBlankEmailId", data.get(1).get(0));

	}
	
	@Then("^Select required Company from the 'Company Apps' list$")
	public void Select_required_Company_from_CompanyAppslist() throws Throwable {

		List<WebElement> listOfCompanies_CompanyApps = UtilityFunctions.retunWebElements("listOfCompanies_CompanyApps");
		System.out.println("Total Companies at Manage Companies : "+listOfCompanies_CompanyApps.size());

		System.out.println("Seleted company to create New Compant App : "+listOfCompanies_CompanyApps.get(0).getText());		
		listOfCompanies_CompanyApps.get(0).click();
					
	}
	
	@When("^Click on 'Add a new App for' button$")
	public void Click_on_AddnewAppfor_button() throws Throwable {

		UtilityFunctions.performAction("buttonAddCompanyApp", "");
		Thread.sleep(3000);
	}
	
	//
	//	@Then("^Keep All Mandatory fields as Blank under 'Add A New App' page$")
	//	public void Keep_All_MandatoryFields_AsBlank_AddNewAppPage(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performAction("fieldAppName", data.get(1).get(1));
	//		UtilityFunctions.performAction("fieldDetOfInt", data.get(2).get(1));
	//
	//	}
	//	
	//	@Then("^Keep All Mandatory fields as Blank under 'Create company' page$")
	//	public void Keep_All_MandatoryFields_AsBlank_CompanyCreation_Page(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performAction("fieldCompanyName", data.get(1).get(1));
	//
	//	}
	//	
	//	@Then("^Verify should display Error msgs for 'Blank Mandatory' fields in 'Create company' Page$")
	//	public void Should_Display_ErrMsg_ForBlankFields_CreateCompany_Page(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		String[] errorMsgActSplit =UtilityFunctions.retunWebElement("errMsgBlankFieldsCreateCompany").getText().split("\n");
	//
	//		for(int k=0; k<errorMsgActSplit.length; k++)
	//		{
	//			Assert.assertEquals(data.get(k).get(0).trim(),errorMsgActSplit[k].trim()); 
	//		}
	//	}
	//
	//	@Then("^Verify should display Error msgs for 'Blank Mandatory' fields in Add New App Page$")
	//	public void Should_Display_ErrMsg_ForBlankFields_AddNewAppPage(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		String[] errorMsgActSplit =UtilityFunctions.retunWebElement("errMsgBlankFieldsAddApp").getText().split("\n");
	//
	//		for(int k=0; k<errorMsgActSplit.length; k++)
	//		{
	//			Assert.assertEquals(data.get(k).get(0).trim(),errorMsgActSplit[k].trim()); 
	//		}
	//	}
	//
	//	@Then("^Enter Valid data in all fields under 'Add A New App' page$")
	//	public void Enter_ValidData_AddNewAppPage(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		appName = data.get(1).get(1);
	//		appDetails = data.get(2).get(1);
	//		UtilityFunctions.performAction("fieldAppName", data.get(1).get(1));
	//		//		UtilityFunctions.performAction("fieldAppName", data.get(1).get(1));
	//		UtilityFunctions.performAction("fieldDetOfInt", data.get(2).get(1));
	//
	//	}
	//
	//	@When("^Click on 'Edit' link beside the 'App Name' field to view the 'Machine Readable Name' field$")
	//	public void Click_On_Edit_link() throws Throwable {
	//
	//		UtilityFunctions.performAction("linkEdit", "");
	//		Thread.sleep(3000);
	//		boolean status = driver.findElement(By.xpath(Hooks.proprty.getProperty("dataMachineRedableName"))).isDisplayed();
	//		if(status==true)
	//		{
	//			System.out.println("'Machine Readable Name' field displayed");
	//		}
	//		else
	//		{
	//			System.out.println("'Machine Readable Name' field Not displayed");
	//		}
	//	}
	//
	//	@Then("^Verify 'Machine Readable Name' field value should match with 'App Name' value by including iphen$")
	//	public void Verify_MachineReadableName_Value() throws Throwable {
	//
	//		String status = driver.findElement(By.xpath(Hooks.proprty.getProperty("dataMachineRedableName"))).getAttribute("value");
	//		if(status.contains("-"))
	//		{
	//			String[] valMRN = status.split("-");
	//			for(int i=0;i<valMRN.length; i++){
	////				if(i==0)
	////				{
	//					if(appName.contains(valMRN[i].substring(0, 1).toUpperCase()+valMRN[i].substring(0, valMRN[i].length())))
	//					{
	//						System.out.println("Machine Readable Name value Matching with App Name");
	//					}
	//					else
	//					{
	//						System.out.println("Machine Readable Name value Not matching with App Name");
	//					}
	//				}
	////				else
	////				{
	////					if(appName.contains(valMRN[i+1].substring(0, 1).toUpperCase()))
	////					{
	////						System.out.println("Machine Readable Name value Matching with App Name");
	////					}
	////					else
	////					{
	////						System.out.println("Machine Readable Name value Not matching with App Name");
	////					}
	////				}
	////			}
	//		}
	//	}
	//
	//	@Then("^Select the require checkbox Options for '(.*)'$")
	//	public void Select_reuire_Checkbox_Option(String text, DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		//		String dataProducts = Arrays.toString(data.toArray());
	//
	//		for(int i=0; i<data.size()-1; i++)
	//		{
	//			String checkboxXpath = "input[value$='"+data.get(i+1).get(0)+"']";
	//			driver.findElement(By.cssSelector(checkboxXpath)).click();
	//		}			
	//	}
	//
	//	@Then("^Select the require Products from the 'Products list'$")
	//	public void Select_reuire_Products_from_Productlist(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		//		String dataProducts = Arrays.toString(data.toArray());
	//
	//		for(int i=0; i<data.size()-1; i++)
	//		{
	//			String checkboxXpath = "input[value$='"+data.get(i+1).get(0)+"']";
	//			driver.findElement(By.cssSelector(checkboxXpath)).click();
	//		}			
	//
	//	}
	//
	//
	//	@Then("^Should display 'Success' msg and verify submitted data in 'My Apps' page$")
	//	public void Should_Display_SuccessMsg_ValidData_AddNewAppPage(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performValidation("pageHeaderMyApps", data.get(1).get(0));
	//		UtilityFunctions.performValidation("successMsgAppCreated", data.get(2).get(0));
	//		UtilityFunctions.performValidation("navigationTabMyApps", data.get(1).get(0));
	//		UtilityFunctions.performValidation("TheseAreyourApps", data.get(3).get(0));
	//
	//		String linkAppNameAtTable = ".//a/strong[text()='"+appName+"']";
	//		String linkAppName = driver.findElement(By.xpath(linkAppNameAtTable)).getText();
	//		Assert.assertEquals(appName,linkAppName);  
	//		//		UtilityFunctions.performValidation(linkAppNameAtTable, appName);
	//
	//
	//	}
	//
	//	@When("^Click on 'App Name' link under 'My Apps' table$")
	//	public void Click_On_AppName_link_Under_MyAppsTable() throws Throwable {
	//
	//		String linkAppNameAtTable = ".//a/strong[text()='"+appName+"']";
	//		driver.findElement(By.xpath(linkAppNameAtTable)).click();
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Should display list of 'My App' tabs under selected 'App Name' link$")
	//	public void Should_display_ListOfTabs_Under_MyApps_link(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		//		# Tabs under My App link
	//		String tabKeys_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Keys']";
	//		String tabProduct_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Products']";
	//		String tabDetails_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Details']";
	//		String tabAnalytics_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Analytics']";
	//		String tabEdit_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[contains(text(),'Edit')]/em//..";
	//		String tabDelete_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[contains(text(),'Delete')]/em//..";
	//
	//		WebElement elmKeys = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabKeys_MyApplink)));
	//		WebElement elmProducts = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabProduct_MyApplink)));
	//		WebElement elmDetails = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabDetails_MyApplink)));
	//		WebElement elmAnalytics = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabAnalytics_MyApplink)));
	//		WebElement elmEdit = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabEdit_MyApplink)));
	//		WebElement elmDelete = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabDelete_MyApplink)));
	//
	//		String textKeys = elmKeys.getText();
	//		String textProduct = elmProducts.getText();
	//		String textDetails = elmDetails.getText();
	//		String textAnalytics = elmAnalytics.getText();
	//		String textEdit = elmEdit.getText();
	//		String textDelete = elmDelete.getText();
	//
	//
	//
	//		Assert.assertEquals(textKeys, data.get(1).get(0));
	//		Assert.assertEquals(textProduct, data.get(2).get(0));
	//		Assert.assertEquals(textDetails, data.get(3).get(0));
	//		Assert.assertEquals(textAnalytics, data.get(4).get(0));
	//		Assert.assertTrue(textEdit.contains(data.get(5).get(0)));
	//		Assert.assertTrue(textDelete.contains(data.get(6).get(0)));
	//
	//		//		elmEdit.click();
	//
	//		//		String editXpath = "//a[contains(text(),'Edit')]/em[text()='"+appName+"']";
	//		//		String deleteXpath = "//a[contains(text(),'Delete')]/em[text()='"+appName+"']";
	//		//		WebElement editEm = new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(editXpath)));
	//		//		WebElement deleteEm = new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(deleteXpath)));
	//		//
	//		//		String appNameAt_Editbutton = editEm.getText();
	//		//		String appNameAt_Deletebutton = deleteEm.getText();
	//		//
	//		//		Assert.assertEquals(appNameAt_Editbutton, data.get(5).get(0));
	//		//		Assert.assertEquals(appNameAt_Deletebutton, data.get(6).get(0));	
	//
	//
	//		//		UtilityFunctions.performValidation(tabKeys_MyApplink, data.get(1).get(0));
	//		//		UtilityFunctions.performValidation(tabProduct_MyApplink, data.get(2).get(0));
	//		//		UtilityFunctions.performValidation(tabDetails_MyApplink, data.get(3).get(0));
	//		//		UtilityFunctions.performValidation(tabAnalytics_MyApplink, data.get(4).get(0));
	//		//		UtilityFunctions.performValidation(tabEdit_MyApplink, data.get(5).get(0));
	//		//		UtilityFunctions.performValidation(tabDelete_MyApplink, data.get(6).get(0));
	//
	//	}
	//
	//	@Then("^Should display 'App Name' at 'Edit' and 'Delete' buttons$")
	//	public void Should_display_AppNameAt_EditandDelete_buttons() throws Throwable {
	//
	//		String editXpath = "//a[contains(text(),'Edit')]/em[text()='"+appName+"']";
	//		String deleteXpath = "//a[contains(text(),'Delete')]/em[text()='"+appName+"']";
	//		WebElement editEm = new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(editXpath)));
	//		WebElement deleteEm = new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfElementLocated(By.xpath(deleteXpath)));
	//
	//		String appNameAt_Editbutton = editEm.getText();
	//		String appNameAt_Deletebutton = deleteEm.getText();
	//
	//		Assert.assertEquals(appName,appNameAt_Editbutton);
	//		Assert.assertEquals(appName,appNameAt_Deletebutton);		
	//
	//
	//	}
	//
	//	@When("^Click on 'Products' tab under 'App Name' link$")
	//	public void Click_On_Products_Tab_Under_AppNamelinks() throws Throwable {
	//
	//		//		WebElement tabProduct = UtilityFunctions.retunWebElement("tabProduct_MyApplink");
	//		String linkAppNameAtTable = ".//a/strong[text()='"+appName+"']";
	//
	//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(linkAppNameAtTable)));
	//		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,-100)", "");
	//		//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", tabProduct);
	//
	//		String tabProduct_MyApplink = "//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Products']";
	//		driver.findElement(By.xpath(tabProduct_MyApplink)).click();
	//
	//		//		UtilityFunctions.performAction("tabProduct_MyApplink", "");
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Verify displayng Product Names matching with Selected Products at 'Create App'$")
	//	public void Verify_Products_With_SelectedProducts(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		listOfProducts = Arrays.toString(data.toArray());
	//
	//		String listOfProducts_ProductTab = " //strong[text()='"+appName+"']//..//..//..//..//..//..//div[contains(text(),'API Product:')]";
	//
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		List<WebElement> listProducts = driver.findElements(By.xpath(listOfProducts_ProductTab));
	//
	//		for(int i=0;i<listProducts.size();i++)
	//		{
	//			textReply.add(listProducts.get(i));
	//		}
	//
	//		System.out.println("Total Records : "+textReply.size());
	//		//		System.out.println("****************Verify Data for "+data.get(k+1).get(0).trim()+"*******************");
	//
	//		if(listProducts.size()==data.size()-1)
	//		{
	//			for(int j=0;j<textReply.size();j++)
	//			{
	//				System.out.println(j+". "+textReply.get(j).getText());
	//				String[] productName = textReply.get(j).getText().trim().split(" ");
	//				//				Assert.assertEquals(data.get(j+1).get(0).trim(), productName[1].trim());
	//				Assert.assertTrue(listOfProducts.contains(productName[2]));
	//
	//			}
	//		}
	//	}
	//
	//	@When("^Click on 'Details' tab under 'App Name' link$")
	//	public void Click_On_Details_Tab_Under_AppNamelinks() throws Throwable {
	//
	//		String linkAppNameAtTable = ".//a/strong[text()='"+appName+"']";
	//
	//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(linkAppNameAtTable)));
	//		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,-100)", "");
	//
	//		String tabDetails_MyApplink = "//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Details']";
	//		driver.findElement(By.xpath(tabDetails_MyApplink)).click();
	//		Thread.sleep(3000);
	//
	//		//		UtilityFunctions.performAction("tabProduct_MyApplink", "");
	//
	//	}
	//
	//	@Then("^Verify displayng App details matching with 'Create New App' details$")
	//	public void Verify_AppDetails_With_CreateNewAppDetails() throws Throwable {
	//
	//		String xpathlistOfTbody = "//strong[text()='"+appName+"']//..//..//..//..//..//..//div/div[2]/table/tbody";
	//
	//		List<WebElement> listOfTbody = 	new WebDriverWait(Hooks.driver,100).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpathlistOfTbody)));
	//		List<WebElement> listOfTr=listOfTbody.get(listOfTbody.size()-1).findElements(By.tagName("tr"));
	//		int k=1;
	//		for(WebElement tr :listOfTr){
	//			System.out.println("Tr Data : "+tr.getText());
	//			if(k==1)
	//			{
	//				int sizeTd = tr.findElements(By.tagName("td")).size();
	//				for(int i=0; i<sizeTd; i++)
	//				{
	//					System.out.println("Td Data : "+tr.findElements(By.tagName("td")).get(i).getText());
	//					String dataDetails = tr.findElements(By.tagName("td")).get(i).getText();
	//					if(i==1)
	//						Assert.assertEquals(appName, dataDetails);
	//				}
	//				k++;
	//			}
	//
	//			else if(k==2)
	//			{
	//				int sizeTd = tr.findElements(By.tagName("td")).size();
	//				for(int i=0; i<sizeTd; i++)
	//				{
	//					System.out.println("Td Data : "+tr.findElements(By.tagName("td")).get(i).getText());
	//					String dataDetails = tr.findElements(By.tagName("td")).get(i).getText();
	//					String[] dataDetailssplit = dataDetails.split("\n");
	//					if(i==1)
	//						for(int m=0; m<dataDetailssplit.length; m++)
	//						{
	//							Assert.assertTrue(listOfProducts.contains(dataDetailssplit[m]));
	//						}
	//				}
	//				k++;
	//			}
	//
	//			else if(k==3)
	//			{
	//				int sizeTd = tr.findElements(By.tagName("td")).size();
	//				for(int i=0; i<sizeTd; i++)
	//				{
	//					System.out.println("Td Data : "+tr.findElements(By.tagName("td")).get(i).getText());
	//					String dataDetails = tr.findElements(By.tagName("td")).get(i).getText();
	//					if(i==1)
	//						Assert.assertEquals(appDetails, dataDetails);
	//				}
	//				k++;
	//			}
	//		}
	//	}
	//
	//	@When("^Click on 'Analytics' tab under 'App Name' link$")
	//	public void Click_On_Analytics_tab_Under_MyAppslink() throws Throwable {
	//
	//		String tabAnalytics_MyApplink = ".//strong[text()='"+appName+"']//..//..//..//..//..//a[text()='Analytics']";
	//		WebElement elmAnalyticsTab = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabAnalytics_MyApplink)));
	//		elmAnalyticsTab.click();
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Should display list of tabs for selected 'Analytics' tab in 'Analytics' page$")
	//	public void Should_display_ListOfTabs_Under_Analytics_page(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performValidation("tabKeys_Analytics", data.get(1).get(0));
	//		UtilityFunctions.performValidation("tabProduct_Analytics", data.get(2).get(0));
	//		UtilityFunctions.performValidation("tabAppDetails_Analytics", data.get(3).get(0));
	//		UtilityFunctions.performValidation("tabEditApp_Analytics", data.get(4).get(0));
	//		UtilityFunctions.performValidation("tabAppPerformance_Analytics", data.get(5).get(0));
	//
	//	}
	//
	//	@When("^Click on 'Products' tab under 'Analytics' page$")
	//	public void Click_On_Products_tab_Under_AnalyticsPage() throws Throwable {
	//
	//		String headerAnalyticsPage = ".//h1[contains(text(),'"+appName+"')]";
	//
	//		((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", driver.findElement(By.xpath(headerAnalyticsPage)));
	//		//		((JavascriptExecutor)driver).executeScript("window.scrollBy(0,-100)", "");
	//
	//		UtilityFunctions.performAction("tabProduct_Analytics", "");
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Verify displaying Product Names matching with Selected Products at 'Create App'$")
	//	public void Verify_ProductsDetails_With_CreateNewAppDetails() throws Throwable {
	//
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		List<WebElement> listReply = UtilityFunctions.retunWebElements("dataProdList_Analytics");
	//		System.out.println("Total Records : "+listReply.size());
	//		for(int i=0;i<listReply.size();i++)
	//		{
	//			textReply.add(listReply.get(i));
	//		}
	//
	//		for(int j=0;j<textReply.size();j++)
	//		{
	//			System.out.println(j+". "+textReply.get(j).getText());
	//			Assert.assertTrue(listOfProducts.contains(textReply.get(j).getText()));
	//		}
	//
	//	}
	//
	//	@When("^Click on 'App Details' tab under 'Analytics' page$")
	//	public void Click_On_AppDetails_tab_Under_AnalyticsPage() throws Throwable {
	//
	//		UtilityFunctions.performAction("tabAppDetails_Analytics", "");
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Verify displaying App Details matching with entered details at 'Create App'$")
	//	public void Verify_AppDetails_Matching_With_CreateNewAppDetails() throws Throwable {
	//
	//		//		Verify App Products
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		List<WebElement> listReply = UtilityFunctions.retunWebElements("dataProducts_DetailsTab");
	//		System.out.println("Total Products : "+listReply.size());
	//		for(int i=0;i<listReply.size();i++)
	//		{
	//			textReply.add(listReply.get(i));
	//		}
	//
	//		for(int j=0;j<textReply.size();j++)
	//		{
	//			if(textReply.get(j).getText().equals("Merchant Boarding � Merchant Boarding"))
	//			{
	//				String valMerchant = textReply.get(j).getText();
	//				System.out.println(j+". "+textReply.get(j).getText());
	//				Assert.assertTrue(listOfProducts.contains(valMerchant.substring(0,  17)));
	//			}
	//			else
	//			{
	//				System.out.println(j+". "+textReply.get(j).getText());
	//				Assert.assertTrue(listOfProducts.contains(textReply.get(j).getText()));
	//			}
	//		}
	//
	//		//		Verify App Name
	//		//		UtilityFunctions.performValidation("dataAppName_DetailsTab", appName);
	//
	//		//		Verify App Details
	//		UtilityFunctions.performValidation("dataAppDetails_Analytics", appDetails);
	//
	//	}
	//
	//	@Then("^Verify 'My Apps' page should contain list of Apps$")
	//	public void Verify_Apps_Exist_ForDeletion(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performValidation("msgExistNotForApps", data.get(1).get(0));
	//	}
	//
	//	@When("^Click on required App Name from the App list for '(.*)'$")
	//	public void Click_On_AppName_link__ForDeletion(String text) throws Throwable {
	//
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		List<WebElement> listReply = UtilityFunctions.retunWebElements("listOfApps");
	//		System.out.println("Total Records : "+listReply.size());
	//		for(int i=0;i<listReply.size();i++)
	//		{
	//			textReply.add(listReply.get(i));
	//		}
	//
	//		//		Click on First App Name
	//		editAppName = textReply.get(0).getText().trim();
	//		deleteAppName = textReply.get(0).getText().trim();
	//		//		System.out.println("Seleted App Name for Edit : "+editAppName);
	//		//		System.out.println("Seleted App Name for Delete : "+deleteAppName);
	//		textReply.get(0).click();
	//
	//	}
	//
	//	@When("^Click on 'Delete' button under App Name link$")
	//	public void Click_On_Delete_button_ForDeletion() throws Throwable {
	//
	//		System.out.println("Seleted App Name for Delete : "+deleteAppName);
	//
	//		String tabDelete_MyApplink = ".//strong[text()='"+deleteAppName+"']//..//..//..//..//..//a[contains(text(),'Delete')]/em//..";
	//		WebElement elmDelete = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabDelete_MyApplink)));
	//
	//		elmDelete.click();
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Application should navigate to Delete App Page by displaying user confirmation as 'Are you sure'$")
	//	public void Should_Navigate_DeletePage() throws Throwable {
	//
	//		UtilityFunctions.performValidation("confirmationDeletePage", "Are you sure?");
	//	}
	//
	//	@When("^Click on 'Delete App' button under 'Delete App' page$")
	//	public void Click_On_Delete_button_Under_DeletePage() throws Throwable {
	//
	//		UtilityFunctions.performAction("buttonDelete", "");
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^App should be Deleted successfully by displaying message as 'App Deleted'")
	//	public void MyApp_ShouldBe_Delete_From_list() throws Throwable {
	//
	//		UtilityFunctions.performValidation("successMsgDelete", "App Deleted!");
	//
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		ArrayList<String> listText = new ArrayList<>();
	//		List<WebElement> listReply = UtilityFunctions.retunWebElements("listOfApps");
	//		System.out.println("Total Records : "+listReply.size());
	//		for(int i=0;i<listReply.size();i++)
	//		{
	//			textReply.add(listReply.get(i));
	//			listText.add(textReply.get(i).getText());
	//		}
	//
	//		//		Verify App Deleted Successfully
	//		if(!listText.contains(deleteAppName))
	//		{
	//			System.out.println("'"+deleteAppName+"'"+" : App Deleted Successfully");	
	//		}
	//		else
	//		{
	//			System.out.println("'"+deleteAppName+"'"+" : App Not Deleted Successfully");
	//		}
	//
	//	}
	//
	//	@When("^Click on 'Edit' button under App Name link$")
	//	public void Click_On_Edit_button_ForUpdate() throws Throwable {
	//
	//		System.out.println("Seleted App Name for Edit : "+editAppName);
	//
	//		String tabEdit_MyApplink = ".//strong[text()='"+deleteAppName+"']//..//..//..//..//..//a[contains(text(),'Edit')]/em//..";
	//		WebElement elmEdit = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(tabEdit_MyApplink)));
	//
	//		elmEdit.click();
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Application should navigate to 'Edit App' Page$")
	//	public void Should_Navigate_EditPage() throws Throwable {
	//
	//		UtilityFunctions.performValidation("pageHeaderEdit", "Edit app");		
	//		UtilityFunctions.performValidation("navigationEditTab", "Edit app");
	//	}
	//
	//	@When("^Click on 'Save App' button under 'Edit App' page$")
	//	public void Click_On_Savebutton_ForUpdate() throws Throwable {
	//
	//		UtilityFunctions.performAction("buttonSave", "");
	//		Thread.sleep(3000);
	//	}
	//
	//	@Then("^Should display 'Success' msg and verify Updated App should listed into the 'My Apps' page$")
	//	public void Should_Display_SuccessMsg_forEdit(DataTable table) throws Throwable {
	//
	//		List<List<String>> data = table.raw();	
	//
	//		UtilityFunctions.performValidation("headerMyApp", data.get(1).get(0));
	//		UtilityFunctions.performValidation("successMsgEdit", data.get(2).get(0));
	//		UtilityFunctions.performValidation("TheseAreyourApps", data.get(3).get(0));
	//
	//		//		Fetch the list of the Apps from the My App page
	//		ArrayList<WebElement> textReply = new ArrayList<>();
	//		ArrayList<String> listText = new ArrayList<>();
	//		List<WebElement> listReply = UtilityFunctions.retunWebElements("listOfApps");
	//		System.out.println("Total Records : "+listReply.size());
	//		for(int i=0;i<listReply.size();i++)
	//		{
	//			textReply.add(listReply.get(i));
	//			listText.add(textReply.get(i).getText());
	//		}
	//
	//		if(listText.contains(appName) && !listText.contains(editAppName))
	//		{
	//			System.out.println("'"+editAppName+"'"+" : App Updated as '"+appName+"' Successfully");
	//		}
	//		else
	//		{
	//			System.out.println("'"+editAppName+"'"+" : App Not Updated as '"+appName+"' Successfully");
	//		}
	//
	//	}

}

