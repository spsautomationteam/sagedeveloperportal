package com.paymentcenter.testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utility.UtilityFunctions;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SignIn {

	public  WebDriver driver;

	public SignIn()
	{
		driver=Hooks.driver;
	}

	@When("^Click on 'Sign In' tab present at top left$")
	public void click_on_Sign_In_tab() throws Throwable {

	//	((JavascriptExecutor)driver).executeScript("scroll(0,200)");
		
		WebElement elementSignIn = driver.findElement(By.xpath(".//*[@id='dexp-dropdown']/ul/li[6]/a"));
		//Function call to Highlight the element
		HighlightElement(driver,elementSignIn);
		
		UtilityFunctions.performAction("tabSignIn", "");
		Thread.sleep(3000);
		
		

	}

	@Then("^Should display 'Sign In' screen along with 'Sign In' fields$")
	public void Sign_In_fields_should_be() throws Throwable {

		UtilityFunctions.performValidation("pageTitle", "Sign In");
		UtilityFunctions.performValidation("labelName", "User Name or e-mail address");
		UtilityFunctions.performValidation("labelPassword", "Password");
		UtilityFunctions.performValidation("buttSignIn", "Sign In");	

	}

	@When("^Enter valid data into User Name or E-mail and Password fields$")
	public void enter_Valid_Data_In_SignIn_Screen(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performAction("fieldName", data.get(1).get(1));
		UtilityFunctions.performAction("fieldPassword", data.get(2).get(1));

	}

	@When("^Click on 'Sign In' button$")
	public void click_on_Sign_In_button() throws Throwable {
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("scroll(0, 250)"); // if the element is on bottom.
		//jse.executeScript("scroll(250, 0)"); // if the element is on top.		
		
		UtilityFunctions.performAction("buttSignIn", "");
		Thread.sleep(1000);
//		try
//		{
////			String sessionExceedMsg = driver.findElement(By.xpath(Hooks.proprty.getProperty("textSessionExpired"))).getText();
//			String sessionExceedMsg = new WebDriverWait(Hooks.driver,10).until(ExpectedConditions.presenceOfElementLocated(By.xpath(Hooks.proprty.getProperty("textSessionExpired")))).getText();
//			if(sessionExceedMsg.equalsIgnoreCase("Session limit exceeded"))
//			{
//				List<WebElement> listRadio = driver.findElements(By.xpath(Hooks.proprty.getProperty("listRadioButtons")));
//				//			listReply.get(listReply.size()-1).click();
//				listRadio.get(1).click();
//				driver.findElement(By.xpath(Hooks.proprty.getProperty("buttonDisconnect"))).click();
//				Thread.sleep(3000);
//			}
//		}
//		catch(Exception e)
//		{
//			System.out.println("Session limit exceeded page Not Exist");
//		}

	}

	@Then("^User should login successfully and Logout button should be display$")
	public void user_should_be_login() throws Throwable {

		UtilityFunctions.performValidation("tabLogout", "");

	}

	@When("^Click on 'Logout' tab in 'Sage Developer User' screen$")
	public void click_on_Logout_tab() throws Throwable {
		//		Thread.sleep(5000);
		UtilityFunctions.performAction("tabLogout", "");
		Thread.sleep(3000);
	}

	@Then("^User should Logout successfully and Sign In tab should be display$")
	public void User_should_Logout() throws Throwable {

		UtilityFunctions.performValidation("tabSignIn", "");

	}

	@When("^Keep all SignIn fields are in Blank$")
	public void keep_all_SignIn_fields_are_in_Blank(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performAction("fieldName", data.get(1).get(1));
		UtilityFunctions.performAction("fieldPassword", data.get(2).get(1));

	}


	@Then("^It should show 'Thank you, your submission has been received.' and Go back to the form  link should be present$")
	public void Should_Display_ThankYou_Msg(DataTable table) throws Throwable {

		List<List<String>> data = table.raw();

		UtilityFunctions.performAction("fieldName", data.get(1).get(1));
		UtilityFunctions.performAction("fieldPassword", data.get(2).get(1));

	}
	
	public static void HighlightElement(WebDriver driver,WebElement element) throws InterruptedException{
		  //Creating JavaScriptExecuter Interface
		   JavascriptExecutor js = (JavascriptExecutor)driver;
		   for (int iCnt = 0; iCnt < 1; iCnt++) {
		      //Execute javascript
		         js.executeScript("arguments[0].style.border='4px groove green'", element);
		         Thread.sleep(1000);
		         js.executeScript("arguments[0].style.border=''", element);
		   }
		 }

}


