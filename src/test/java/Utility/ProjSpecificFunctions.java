package Utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.paymentcenter.testcases.Hooks;


public class ProjSpecificFunctions {
	
	public static String TR_REFERENCE = "QQQQQQQQQQ";
	public static String TR_ORDERNO;
	
	public static void doACHSaleTransaction(String bodyData) throws IOException, InterruptedException, AWTException{
		
		UtilityFunctions.performAction("panelACH","");	
		UtilityFunctions.performAction("panelPostCharges","");
		WebElement we = UtilityFunctions.retunWebElement("panelPostChargesBody");
		we.click();	
		
	    Thread.sleep(2000);
		Robot robot = new Robot();
		
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_DELETE);

		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_DELETE);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_A);
		
		
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    //Set the String to Enter
        //String bodyData = Hooks.proprty.getProperty("postChargeBodyData");
	    StringSelection stringSelection = new StringSelection(bodyData);
	    //Copy the String to Clip board

	    clipboard.setContents(stringSelection, null);
	    //Use Robot class instance to simulate CTRL+C and CTRL+V key events :
        
	    Thread.sleep(2000);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    //Simulate Enter key event
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    
	    Thread.sleep(2000);
	    Utility.UtilityFunctions.performAction("btnSendThisRequest", "");
	    Thread.sleep(5000);
	    
	    WebElement responeBody = UtilityFunctions.retunWebElement("panelResponseMessage");
	    String responseMsg = responeBody.getText();
	    int posOfReference = responseMsg.indexOf("reference");
	    TR_REFERENCE = responseMsg.substring(posOfReference + 13, posOfReference + 23);
	    
	    int posOfOrderNo = responseMsg.indexOf("orderNumber");
	    TR_ORDERNO = responseMsg.substring(posOfOrderNo + 15, posOfOrderNo + 25);
	    
	    System.out.println("Reference No is = "+TR_REFERENCE);
	    System.out.println("Order No No is = "+TR_ORDERNO);
	    
	    Utility.UtilityFunctions.performAction("linkApiSandBox", "");
		Thread.sleep(2000);
	}

	
	public static void doBankCardSaleTransaction(String bodyData) throws IOException, InterruptedException, AWTException{
		
		UtilityFunctions.performAction("panalBankCard","");	
		UtilityFunctions.performAction("panelPostCharges","");
		UtilityFunctions.performAction("panelQueryParameter","");
		
		WebElement we = UtilityFunctions.retunWebElement("panelPostChargesBody");
		we.click();	
		
	    Thread.sleep(2000);
		Robot robot = new Robot();
		
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_A);
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_DELETE);

		Thread.sleep(2000);
		robot.keyRelease(KeyEvent.VK_DELETE);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_A);
		
		
	    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    //Set the String to Enter
        //String bodyData = Hooks.proprty.getProperty("postChargeBodyData");
	    StringSelection stringSelection = new StringSelection(bodyData);
	    //Copy the String to Clip board

	    clipboard.setContents(stringSelection, null);
	    //Use Robot class instance to simulate CTRL+C and CTRL+V key events :
        
	    Thread.sleep(2000);
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_V);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    //Simulate Enter key event
	    robot.keyPress(KeyEvent.VK_ENTER);
	    robot.keyRelease(KeyEvent.VK_ENTER);
	    
	    Thread.sleep(2000);
	    Utility.UtilityFunctions.performAction("btnSendThisRequest", "");
	    Thread.sleep(5000);
	    
	    WebElement responeBody = UtilityFunctions.retunWebElement("panelResponseMessage");
	    String responseMsg = responeBody.getText();
	    int posOfReference = responseMsg.indexOf("reference");
	    TR_REFERENCE = responseMsg.substring(posOfReference + 13, posOfReference + 23);
	    
	    int posOfOrderNo = responseMsg.indexOf("orderNumber");
	    TR_ORDERNO = responseMsg.substring(posOfOrderNo + 15, posOfOrderNo + 25);
	    
	    System.out.println("Reference No is = "+TR_REFERENCE);
	    System.out.println("Order No No is = "+TR_ORDERNO);
	    
	    Utility.UtilityFunctions.performAction("linkApiSandBox", "");
		Thread.sleep(2000);
	}

	
}
