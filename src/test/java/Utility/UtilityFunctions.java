package Utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.paymentcenter.testcases.Hooks;



public class UtilityFunctions
{
	public static String strTxType;
	public static String parentWindow;


	//*********************************************************************
	// function to create WebElement
	//*********************************************************************   
	public static WebElement retunWebElement(String strObj) throws IOException
	{
		//Hooks.filereadConfig();
		WebElement element =null;
		String strObjProp =  Hooks.proprty.getProperty(strObj);
		String[] propParameters = strObjProp.split(",",2);
		String strKey = propParameters[0];
		String strValue = propParameters[1];

		switch(strKey.toLowerCase())
		{
		case "xpath":
			element = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfElementLocated(By.xpath(strValue)));
			//((JavascriptExecutor) Hooks.driver).executeScript("arguments[0].scrollIntoView(true);", element);
			break;

		case "id":
			element =Hooks.driver.findElement(By.id(strValue));
			break;
		case "name":
			element =Hooks.driver.findElement(By.name(strValue));
			break;
		case "cssselector":
			element =Hooks.driver.findElement(By.cssSelector(strValue));
			break;
		case "default":
			element=null;
		}

		return element;
	}
	
	public static List<WebElement> retunWebElements(String strObj) throws IOException
	{
		//Hooks.filereadConfig();
		List<WebElement> elements =null;
		String strObjProp =  Hooks.proprty.getProperty(strObj);
		String[] propParameters = strObjProp.split(",",2);
		String strKey = propParameters[0];
		String strValue = propParameters[1];

		switch(strKey.toLowerCase())
		{
		case "xpath":
			elements = new WebDriverWait(Hooks.driver,30).until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(strValue)));
			//((JavascriptExecutor) Hooks.driver).executeScript("arguments[0].scrollIntoView(true);", element);
			break;

		case "id":
			elements =Hooks.driver.findElements(By.id(strValue));
			break;
		case "name":
			elements =Hooks.driver.findElements(By.name(strValue));
			break;
		case "cssselector":
			elements =Hooks.driver.findElements(By.cssSelector(strValue));
			break;
		case "default":
			elements=null;
		}

		return elements;
	}



	//*********************************************************************
	//Create an element and then perform the respective action
	//*********************************************************************

	public static void performAction(String strObjct,String data) throws IOException, InterruptedException, AWTException
	{
		WebElement we = UtilityFunctions.retunWebElement(strObjct);
		
		((JavascriptExecutor) Hooks.driver).executeScript("arguments[0].scrollIntoView(false);", we);
		
		String tagName = we.getTagName();
		switch(tagName.toLowerCase())
		{
		case "button":
			we.click();                          
			break;
		case "a":
			we.click();          

			break;
		case "span":
			if(data.equals(""))                                                          
			{
				we.click();          
				break;
			}
			else
			{
				we.clear();
				we.sendKeys(data);
				break;
			}			
			
		case "li":
			we.click(); 
			
		case "strong":
			we.click();    

			break;
		case "label":
			we.click();          
			break;
		case "input":
//			String type = we.getAttribute("type");
//			if(data.equals("") && type.equals("checkbox"))
//			{
//				
//			}
			if(data.equals(""))                                                          
			{
				we.click();          
				break;
			}
			else
			{
				we.clear();
				we.sendKeys(data);
				break;
			}                                                             
		case "textarea":
			we.clear();
			we.sendKeys(data);
			break;
		case "select":
			Select se=new Select(we);                                                         
			se.selectByVisibleText(data);
			break;
		case "div":
			we.click();          

			Thread.sleep(2000);
			Robot robot = new Robot();

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_A);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_DELETE);

			Thread.sleep(2000);
			robot.keyRelease(KeyEvent.VK_DELETE);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_A);


			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			//Set the String to Enter

			StringSelection stringSelection = new StringSelection(data);
			//Copy the String to Clipboard

			clipboard.setContents(stringSelection, null);
			//Use Robot class instance to simulate CTRL+C and CTRL+V key events :

			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			//Simulate Enter key event
			robot.keyPress(KeyEvent.VK_ENTER);


			break;
		case "p":
			we.click();                          
			break;  

		case "pre":

			Thread.sleep(2000);



			//we.sendKeys(data);
			break;  

		}

	}


	//*********************************************************************
	//Create an element and then perform the respective validation
	//*********************************************************************

	public static void performValidation(String strObjct,String data) throws IOException, InterruptedException
	{
		WebElement we = UtilityFunctions.retunWebElement(strObjct);
		String tagName = we.getTagName();
		switch(tagName.toLowerCase())
		{
		case "strong":
			//Assert.assertTrue(we.getText().contentEquals(data)); 
			//Assert.assertEquals(data,we.getText());
			//Assert.assertTrue(we.getText().contentEquals(data));
			//System.out.println(we.getText());
			//System.out.println(data);
			Assert.assertTrue(we.getText().contains(data));
			break; 

		case "div":
			Assert.assertTrue(we.getText().contains(data));  
			break;
			
		case "em":
			// Assert.assertEquals(data,we.getText()); 
		   //  JavascriptExecutor js = (JavascriptExecutor)Hooks.driver;
			JavascriptExecutor js = (JavascriptExecutor)Hooks.driver;
			// Navigate to new Page
			String val =(String)js.executeScript("return arguments[0].innerHTML;",we);
			System.out.println("enclosing_type.enclosing_method() val ;;"+val);
			break;

		case "input":
			String type = we.getAttribute("type");
			if(type.equals("text"))
			{
				Assert.assertEquals(data,we.getAttribute("value"));  
				break;
			}
			else if(type.equals("email"))
			{
				Assert.assertEquals(data,we.getAttribute("value"));  
				break;
			}
			else if(type.equals("submit"))
			{
				Assert.assertEquals(data,we.getAttribute("value"));  
				break;
			}
			else
			{
				Assert.assertEquals(data,we.getAttribute("checked"));  
				break;	
			}

		case "select":
			Select se=new Select(we);			
			WebElement valCountry = se.getFirstSelectedOption();
			Assert.assertEquals(data,valCountry.getText());  
			break;

		case "code":
			Assert.assertTrue(we.getText().contains(data));
			//System.out.println(we.getText());
			break;
		case "h1":
			Assert.assertEquals(data,we.getText()); 
			break;
		case "button":
			boolean b=Boolean.parseBoolean(data); ;
			Assert.assertEquals(b,we.isEnabled());  
			break;
		case "ul":
			Assert.assertEquals(data,we.getText()); 
			break;

		case "pre":
			//System.out.println(we.getText());
			//System.out.println(data);
			Assert.assertTrue(we.getText().contains(data));
			break;
			
		case "label":
			//System.out.println(we.getText());
			//System.out.println(data);
			Assert.assertTrue(we.getText().contains(data));
			break;
		}


	}


	//*********************************************************************
	//Wait until page loads
	//*********************************************************************

	public static void waitForPageToBeReady() throws InterruptedException
	{
		WebDriver driver =Hooks.driver;
		JavascriptExecutor js = (JavascriptExecutor)driver;

		//This loop will rotate for 100 times to check If page Is ready after every 1 second.
		//You can replace your if you wants to Increase or decrease wait time.
		for (int i=0; i<400; i++)
		{
			Thread.sleep(1000);             
			//To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete"))
			{
				break;
			}  
		}
	}




	//*********************************************************************
	//Switch between browser tabs as per the given tab index
	//*********************************************************************

	public static void switchBetweenTabs(int tabNum) throws InterruptedException
	{
		Thread.sleep(5000);
		ArrayList<String> tabs2 = new ArrayList<String> (Hooks.driver.getWindowHandles());
		Hooks.driver.switchTo().window(tabs2.get(tabNum));
		Thread.sleep(5000);
		waitForPageToBeReady();

	}

	public static void switchTabs() throws InterruptedException
	{
		parentWindow = Hooks.driver.getWindowHandle();
		System.out.println(parentWindow);
		Set<String> handles =  Hooks.driver.getWindowHandles();
		for(String windowHandle  : handles)
		{
			if(!windowHandle.equals(parentWindow))
			{
				System.out.println(windowHandle);
				Hooks.driver.switchTo().window(windowHandle);

			}
		}
	}


	//*********************************************************************
	//Navigating to Accept Payment screen
	//*********************************************************************
	public static void navigatePaymentScreen(String menu) throws Exception
	{
		performAction("menuMerchantAccount","");  
		Thread.sleep(2000);
		performAction("menuPayments","");   
		//Actions act=new Actions(Hooks.driver);
		//act.moveToElement(UtilityFunctions.retunWebElement("menuPayments")).click();
		Thread.sleep(2000);      

		performAction(menu,"");
		//Actions act1=new Actions(Hooks.driver);
		//act1.moveToElement(UtilityFunctions.retunWebElement(menu)).click().build().perform();;
		Thread.sleep(2000);

		//UtilityFunctions.switchBetweenTabs(1);
		UtilityFunctions.switchTabs();

		waitForPageToBeReady();
		Assert.assertEquals("Sage Login",Hooks.driver.getTitle());          

		performAction("txtAcceptPymntEmail",ExcelFunctions.getCellData("Email",1));
		performAction("txtAcceptPymntPassword",ExcelFunctions.getCellData("Password",1));
		performAction("btnAcceptPymntSignIn","");
		waitForPageToBeReady();
		Thread.sleep(10000);
		Assert.assertEquals("Virtual Terminal",Hooks.driver.getTitle());

	}


	//*********************************************************************
	//Reading table row data
	//*********************************************************************
	public static boolean readTable(String strObj1,String data) throws IOException
	{

		WebElement table_element = UtilityFunctions.retunWebElement(strObj1);                                      
		String strObjProp =  Hooks.proprty.getProperty(strObj1);
		String[] propParameters = strObjProp.split(",",2);
		String strKey = propParameters[0];
		String strValue = propParameters[1];
		//if(table_element.isEnabled())
		{
			List<WebElement> tr_collection=Hooks.driver.findElements(By.xpath(strValue+"/tbody/tr"));

			//System.out.println("NUMBER OF ROWS IN THIS TABLE = "+tr_collection.size());

			for(WebElement trElement : tr_collection)
			{
				List<WebElement> td_collection=trElement.findElements(By.xpath("td"));

				for(WebElement tdElement : td_collection)
				{

					if(data.equals(tdElement.getText()))
					{
						System.out.println(tdElement.getText());
						return true;

					}

				}

			}
		}

		return false;      
	}
	//*********************************************************************
	//Perform an action on table data
	//*********************************************************************
	public static void TableAction(String strObj1,String data) throws IOException, InterruptedException
	{

		String strObjProp =  Hooks.proprty.getProperty(strObj1);
		String[] propParameters = strObjProp.split(",",2);
		String strValue = propParameters[1];
		//if(table_element.isEnabled())
		{
			List<WebElement> tr_collection=Hooks.driver.findElements(By.xpath(strValue+"/tbody/tr"));
			Found:
			{
				for(WebElement trElement : tr_collection)
				{
					List<WebElement> td_collection=trElement.findElements(By.xpath("td"));

					for(WebElement tdElement : td_collection)
					{

						String tagName = tdElement.getTagName();
						switch(tagName.toLowerCase())
						{
						case "a":
							if(data.equals(tdElement.getText()))
							{                                                             
								Hooks.driver.findElement(By.partialLinkText(data)).click();
								break Found;

							}
							break;
						case "input":
							if(data.equals(""))                                                          
							{
								tdElement.click();           
								break;
							}                             
							break;

						}



					}

				}
			}
		}                                                                                            
	}


	//*********************************************************************
	//Perform a credit card transaction
	//*********************************************************************
	public static void performCCTransaction(String TxType,String CardNo) throws Exception
	{

		Hooks.driver.navigate().refresh();
		UtilityFunctions.waitForElementNotVisible(50000,Hooks.driver,".//div[@class='loading-spinner-holder ng-scope']");

		strTxType = TxType; //ExcelFunctions.getCellData("strCCTxType",1);

		performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));
		performAction("selectTxType",strTxType);
		if(strTxType.equals("Force"))
		{
			performAction("txtAuthCode",ExcelFunctions.getCellData("strAuthCode",1));
		}
		performAction("txtSubTotal",ExcelFunctions.getCellData("strSubTotal",1));

		WebElement toClear = UtilityFunctions.retunWebElement("txtShipping");
		toClear.sendKeys(Keys.CONTROL + "a");
		toClear.sendKeys(Keys.DELETE);
		performAction("txtShipping",ExcelFunctions.getCellData("strShipping",1));

		WebElement toClear1 = UtilityFunctions.retunWebElement("txtTax");
		toClear1.sendKeys(Keys.CONTROL + "a");
		toClear1.sendKeys(Keys.DELETE);
		performAction("txtTax",ExcelFunctions.getCellData("strTax",1));
		//Assert.assertEquals("");                                           
		performAction("txtCardNumber",CardNo);//ExcelFunctions.getCellData("strCardNumber",1));                                               
		performAction("cmbExpirationYear","");
		performAction("listExpYearData","");

		performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
		performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
		performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
		//performAction("txtBillingPhone",ExcelFunctions.getCellData("strPhone",1));
		performAction("txtBillingAddress",ExcelFunctions.getCellData("strAddress",1));
		performAction("txtBillingCity",ExcelFunctions.getCellData("strCity",1));
		performAction("selectBillingState",ExcelFunctions.getCellData("strState",1));
		performAction("txtBillingZip",ExcelFunctions.getCellData("strZip",1));
		performAction("selectBillingCountry",ExcelFunctions.getCellData("strCountry",1));                                        
		performAction("txtBillingNotes",ExcelFunctions.getCellData("strNotes",1));                                      
		performAction("btnSubmit","");
		UtilityFunctions.waitForPageToBeReady();


	}


	//*********************************************************************
	//Perform a ACH transaction
	//*********************************************************************
	public static void performACHTransaction(String TxType) throws Exception
	{
		UtilityFunctions.waitForPageToBeReady();
		Hooks.driver.navigate().refresh();
		Thread.sleep(5000);
		performAction("labelVirtualCheck","");

		strTxType = TxType;//ExcelFunctions.getCellData("strACHTxType",1);
		performAction("txtOrderNum",ExcelFunctions.getCellData("strOrderNum",1));
		performAction("selectTxType",strTxType);
		performAction("txtSubTotal",ExcelFunctions.getCellData("strSubTotal",1));
		performAction("txtShipping",ExcelFunctions.getCellData("strShipping",1));
		performAction("txtTax",ExcelFunctions.getCellData("strTax",1));
		//Assert.assertEquals("");                                           
		performAction("txtRoutingNumber",ExcelFunctions.getCellData("strRoutingNumber",1));
		performAction("txtAccountNumber",ExcelFunctions.getCellData("strAccountNumber",1));
		performAction("selectAccountType",ExcelFunctions.getCellData("strAccountType",1));
		performAction("selectOriginatorID",ExcelFunctions.getCellData("strOriginatorID",1));                                                   

		performAction("txtBillingFirstName",ExcelFunctions.getCellData("strFirstName",1));
		performAction("txtBillingLastName",ExcelFunctions.getCellData("strLastName",1));
		performAction("txtBillingEmail",ExcelFunctions.getCellData("strEmail",1));
		//performAction("txtBillingPhone",ExcelFunctions.getCellData("strPhone",1));
		performAction("txtBillingAddress",ExcelFunctions.getCellData("strAddress",1));
		performAction("txtBillingCity",ExcelFunctions.getCellData("strCity",1));
		performAction("selectBillingState",ExcelFunctions.getCellData("strState",1));
		performAction("txtBillingZip",ExcelFunctions.getCellData("strZip",1));
		performAction("selectBillingCountry",ExcelFunctions.getCellData("strCountry",1));                                                        
		performAction("txtBillingNotes",ExcelFunctions.getCellData("strNotes",1));                                                      
		performAction("btnSubmit","");

		UtilityFunctions.waitForPageToBeReady();

	}


	//*********************************************************************
	//return current year in 2 digits
	//*********************************************************************

	public static String calculateYear()
	{
		DateFormat df = new SimpleDateFormat("yy"); // Just the year, with 2 digits
		String formattedDate = (df.format(Calendar.getInstance().getTime())+2);
		System.out.println(formattedDate);
		return formattedDate;
	}


	//*********************************************************************
	//Wait for element to disappear
	//*********************************************************************                   
	public static String waitForElementNotVisible(int timeOutInSeconds, WebDriver driver, String elementXPath) {
		if ((driver == null) || (elementXPath == null) || elementXPath.isEmpty()) {

			return "Wrong usage of WaitforElementNotVisible()";
		}
		(new WebDriverWait(driver, timeOutInSeconds)).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(elementXPath)));
		return null;
	}


	//*********************************************************************
	//Perform mouse hover
	//*********************************************************************                   
	public static void mouseOver(String locator) throws IOException {


		WebElement el = UtilityFunctions.retunWebElement(locator);

		if (el == null) {
			throw new IllegalStateException(String.format("ERROR: Element %s not found",locator));
		}

		new Actions(Hooks.driver).moveToElement(el).perform();
	}



	/**
	 * Returns true if there are no elements with the 'spinner' class name.
	 */
	public static final ExpectedCondition<Boolean> EXPECT_NO_SPINNERS = new ExpectedCondition<Boolean>() {
		@Override
		public Boolean apply(WebDriver driver) {
			Boolean loaded = true;
			try {
				List<WebElement> spinners = driver.findElements(By.className("spinner"));
				for (WebElement spinner : spinners) {
					if (spinner.isDisplayed()) {
						loaded = false;
						break;
					}
				}
			}catch (Exception ex) {
				loaded = false;
			}
			return loaded;
		}
	};
}