 @Devportal
Feature: Verify Partners page
 I want to this template for my feature file
 
 @Partners_Tab
 Scenario: Verify user able to login into 'Sage Developer Portal' with valid login credentials
 Given Launch Sage Developer portal URL
 When Click on 'API SandBox' tab present at top of the Page
 Then Should navigate to 'API Documentation' page properly
 When Click on 'ACH' API link
 Then Should navigate to 'ACH' API page properly
 When Click on each and every ACH API transaction links
 Then Should display each 'ACH API transaction page' without 'Page Not Found' issue
 
 