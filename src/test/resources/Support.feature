@Devportal
Feature: Verify All Features under Support Page/Tab
 I want to this template for my feature file
 
 @Validate_AllOptions_Under_Support_Page_WithOut_SignIn
 Scenario: Verify displaying 'controls'at Support page 
 Given Launch Sage Developer portal URL
 When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 Then Verify It should display the required options under 'Support' page
 |Fields 				 |
 |Contact Support| 
 |Submit				 |
 
 @Validate_EmailField_Under_Support_Page_With_SignIn
 Scenario: Verify Email Field should contain Email Id when user Signed In 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 		|
 |Username 	 	 |sri555 				|
 |Password  	 |midhun@123		|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 Then Verify 'Email field' should be filled by the Signed Email
 |Email 				 				 |
 |santosh.kb@sonata-software.com |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 #@Validate_EmailField_WithInvalidData_SupportPage
 #Scenario: Verify Warning message for Email field with invalid data 
 #Given Launch Sage Developer portal URL
 #When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 #And Enter invalid Data into 'Email' field
 #|Email 				 				 |
 #|fsdfsdfsfffsdf         |
 #And Click on Submit button
 #Then Should display warning message like 'Please enter an email address.'
 #
 #@Validate_EmailFieldWithData_BlankDescriptionField
 #Scenario: Verify Warning message for Blank 'Description' field
 #Given Launch Sage Developer portal URL
 #When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 #Then Enter Valid Email Id and Keep 'Description' field as Blank
 #|Fields   		 |Value 		 			        |
 #|Email   	 	 |kumar.midhuna@gmail.com |
 #|Description  |								        |
 #And Click on Submit button
 #Then Should display warning message like 'Please enter an email address.'
 
 @Validate_ValidData_Submit_And_ThankYou_Message_Without_SignIn
 Scenario: Verify Support page functionality with Valid data 
 Given Launch Sage Developer portal URL
 When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 When Enter Valid data into all available fields
 |Fields   		 |Value 		 			        |
 |Email   	 	 |boddeda.kumar@sage.com  |
 |Description  |Test Description        |
 And Click on Submit button
 Then Should show 'Thank you, your submission has been received.' and Go back to the form  link should be present
 
 @Validate_GoBack_button_In_ThankYou_Message_Page
 Scenario: Verify 'Go back to the form' link functionality
 Given Launch Sage Developer portal URL
 When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 When Enter Valid data into all available fields
 |Fields   		 |Value 		 			        |
 |Email   	 	 |boddeda.kumar@sage.com |
 |Description  |Test Description        |
 And Click on Submit button
 Then Should show 'Thank you, your submission has been received.' and Go back to the form  link should be present
 When Click on Go Back button
 Then It should Navigate/Go Back to the 'Contact Support form'
 
 @Validate_WarningMsg_For_ExceedData_Into_DescriptionField
 Scenario: Verify displaying warning message for Description field
 Given Launch Sage Developer portal URL
 When Click on 'Support' tab in 'Sage Developer User' screen present at top left
 When Enter data into Description field Exceed the limit
 |Fields   		 |Value 		 			        |
 |Description  |fklkjklfklsjfklsdlgfsdfjskdlfjlkfjflfjsdklfsdjlkfsdjlkfsdjflksdfjsdlkfjsdlkfjsdlkfsdjklfsdjfklsdjfsdlkfjsdlkfjsdklfjsdklfjsdklfjsdlkfjsdklfjsdklfjsdklfjsdklfjdsklfsdjklfsdjfklsdjflksdjfklsdfsdlkfjsdlkfjsdlkfjsdkljfsdlkfjsdklfjsdlkfjsdklfjsdlkfjsdlkfjsdlkfjlksdfjsdklfjsdkfjjfklsjklfskldjfklsdjfklsdjflksdfjsdlkfjsdlkfjsdklfjsdkldjfklfjslkjdslkfdsjflkdslkfjsdklfjklsdjfklsdjfklsdfjskdljsdklfjklfsdjfklsdfjsdklfjkljfsdkldsjsdkjlkjfklsdjfklsdjflksdjfklsdjfsldkfjsdklfjswlkfjsdklfjsdlkfjsdlkfjsdlfjfsfl@9n        |
 And Click on Submit button
 Then It should show warning message 'Content limited to 500 characters, remaining: 0'
 
 
 