@Devportal
Feature: Verify All Features under 'New Company' & 'Company Apps' Page
 I want to this template for my feature file
 
 @Validate_AllOptions_Under_ManageCompanies_Page
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 			|
 |Username 	 	 |sri555 |
 |Password  	 |midhun@123			|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 Then Verify It should display the required options under 'My Apps' page
 |Fields 				 |
 |My Apps				 | 
 |My Apps (Individual)				   |
 |Company Apps (Team)            |
 |Manage Companies / Invitations |
 |Looks like you don�t have any apps. Get started by adding one.|
 |Add a new App |
 When Click on 'Manage Companies/Invitations' link
 Then Verify It should display the required options under 'Manage Companies/Invitations' page
 |Fields 				   |
 |Manage Companies | 
 |Companies			   |
 |CREATE COMPANY   |
 |Invitations      |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Validate_AllOptions_Under_AddNewComapny_Page
 Scenario: Verify displaying 'controls'at 'Add New Comapny' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555       |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Manage Companies/Invitations' link
 And Click on 'CREATE COMPANY' button
 Then Verify It should display the required options under 'Create Company' page
 |Fields 				 |
 |Create company | 
 |Company Name	 |
 |Save company   |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_AddNewComapny_With_ValidData
 Scenario: Verify displaying 'controls'at 'Add New Comapny' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555       |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Manage Companies/Invitations' link
 And Click on 'CREATE COMPANY' button
 Then Verify It should display the required options under 'Create Company' page
 |Fields 				 |
 |Create company | 
 |Company Name	 |
 |Save company   |
 When Enter value into Company Name field
 And Click on 'Save Company' button
 Then Should display 'Success' msg and verify submitted data in 'Create company' page
 |VerifyValues|
 |Create company |
 |Company Created|
 And Verify created new company should listed under Company Apps and Manage Companies links
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_AddNewComapny_With_BlankData
 Scenario: Verify displaying 'controls'at 'Add New Comapny' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555       |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Manage Companies/Invitations' link
 And Click on 'CREATE COMPANY' button
 Then Verify It should display the required options under 'Create Company' page
 |Fields 				 |
 |Create company | 
 |Company Name	 |
 |Save company   |
 When Keep Company Name field as Blank and Click on 'Save Company' button 
 Then Should display 'Error' msgs for blank 'Company Name' and 'Internal Name' fields
 |Company Name field is required. |
 |Internal Name field is required.|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display 
 
 @Verify_CreateUserInvitions_Functionality_With_ValidData
 Scenario: Verify_CreateUserInvitions_Functionality_With_ValidData 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
# And Click on 'Manage Companies/Invitations' link
 And Select required Company from the Manage Companies list 
 And Enter Email Id into 'User Email' for sending Invitation
 And Click on 'Invite' button
 Then Verify Success for User Invitation and Other Data
|Success Msg |
|Developer invited successfully. |
|Pending Invitations 						 |
|CANCEL        									 |
When Click on 'Logout' tab in 'Sage Developer User' screen
Then User should Logout successfully and Sign In tab should be display 

@Verify_Cancel_Functionality_for_UserInvitions
 Scenario: Verify_Cancel_Functionality_for_UserInvitions 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the Manage Companies list 
 And Enter Email Id into 'User Email' for sending Invitation
 And Click on 'Invite' button
 Then Verify Success for User Invitation and Other Data
|Success Msg |
|Developer invited successfully. |
|Pending Invitations 						 |
|CANCEL        									 |
When Click on 'Cancel' button to cancel the 'Developer' Invitation
Then Verify confirmation details for Cancel Invitation
|Confirmation Msg |
|You will be cancelling the invitation that was sent to|
|Cancel Invitation |
|Cancel 					 |
When Click on 'Cancel Invitaion' button
Then Verify Success Msg for 'Cancel Invitation'
When Click on 'Logout' tab in 'Sage Developer User' screen
Then User should Logout successfully and Sign In tab should be display  
 
@Verify_UserInvitions_With_Blank_UserEmailId
 Scenario: Verify_UserInvitions_With_Blank_UserEmailId 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the Manage Companies list 
 And Keep 'User Email' field Blank and Click on 'Invite' button
 Then Verify Error Msg for Blank 'User Email' field
|Error Msg |
|User's Email field is required. |
When Click on 'Logout' tab in 'Sage Developer User' screen
Then User should Logout successfully and Sign In tab should be display 

@Validate_AllControls_Under_AddNewCompanyApp_Page_SignIn
 Scenario: Validate_AllControls_Under_AddNewCompanyApp_Page_SignIn 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555       |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the 'Company Apps' list 
 And Click on 'Add a new App for' button 
 Then Verify It should display the required options under 'Add A New App' page
 |Fields 				 |
 |App Name				 | 
 |Details of Integration	 |
 |Integration Security     |
 |Product									 |
 |Create App 							 | 
 |Add App									 |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
@Verify_AllOptions_Keep_Blank_AddNewCompanyAppPage
 Scenario: Verify_AllOptions_Keep_Blank_AddNewCompanyAppPage 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555 |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the 'Company Apps' list 
 And Click on 'Add a new App for' button 
 And Keep All Mandatory fields as Blank under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 | 			| 
 |Details of Integration	 | 			| 
 And Click on 'Create App' button
 Then Verify should display Error msgs for 'Blank Mandatory' fields in Add New App Page
 |App Name field is required.|
 |Machine-readable name field is required.|
 |Details of Integration field is required.|
 |Product field is required.|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display

@Verify_AddNewCompanyApp_Functionality_With_ValidData
 Scenario: Verify_AddNewCompanyApp_Functionality_With_ValidData
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the 'Company Apps' list 
 And Click on 'Add a new App for' button 
 And Enter Valid data in all fields under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 |WebEx App     | 
 |Details of Integration	 |WebEx Details	|
And Click on 'Edit' link beside the 'App Name' field to view the 'Machine Readable Name' field
Then Verify 'Machine Readable Name' field value should match with 'App Name' value by including iphen
 # As per xpath syntax, should give - symbol before checkbox value(Ex : -Tokens)
 And Select the require drop down item for 'Integration Security'  
 And Select the require Products from the 'Products list'
  |CheckboxName|
	|Bankcard |
	|Bankcard and Token |
	|Merchant Boarding |
	|-PaymentsJS |
	|-Token | 
 And Click on 'Create App' button
 Then Should display 'Success' msg and verify submitted data in 'My Apps' page
 |VerifyValues|
 |My Apps |
 |App Created!|
 |These are your apps! Explore them!|
 When Click on 'App Name' link under 'My Apps' table
 Then Should display list of 'My App' tabs under selected 'App Name' link
 |TabNames 	|
 |Keys	|
 |Products|
 |Details	|
 |Analytics|
 |Edit |
 |Delete|
 And Should display 'App Name' at 'Edit' and 'Delete' buttons
 When Click on 'Products' tab under 'App Name' link
 Then Verify displayng Product Names matching with Selected Products at 'Create App'
 |CheckboxName|
 |Bankcard |
 |Bankcard and Token |
 |Merchant Boarding |
 |PaymentsJS |
 |Token | 
 When Click on 'Details' tab under 'App Name' link
 Then Verify displayng App details matching with 'Create New App' details
 When Click on 'Analytics' tab under 'App Name' link 
 Then Should display list of tabs for selected 'Analytics' tab in 'Analytics' page
 |TabNames 	|
 |Keys	|
 |Products|
 |App Details	|
 |Analytics|
 |Edit App|
 |App Performance|
 When Click on 'Products' tab under 'Analytics' page
 Then Verify displaying Product Names matching with Selected Products at 'Create App'
 When Click on 'App Details' tab under 'Analytics' page
 Then Verify displaying App Details matching with entered details at 'Create App'
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_Edit_Functionality_ForExisting_MyCompanyApp
 Scenario: Verify_Edit_Functionality_ForExisting_MyCompanyApp 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Select required Company from the 'Company Apps' list 
 Then Verify 'My Apps' page should contain list of Apps 
 |Apps Message |
 |These are your apps! Explore them!|
 When Click on required App Name from the App list for 'Updation'
 And Click on 'Edit' button under App Name link
 Then Application should navigate to 'Edit App' Page
 And Enter Valid data in all fields under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 |Skype App        | 
 |Details of Integration	 |Skype App Details	   |
 And Click on 'Save App' button under 'Edit App' page
 Then Should display 'Success' msg and verify Updated App should listed into the 'My Apps' page
 |VerifyValues|
 |My Apps |
 |App Updated!|
 |These are your apps! Explore them!|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_Delete_Functionality_ForExisting_CompanyApp
 Scenario: Verify_Delete_Functionality_ForExisting_CompanyApp
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
  And Select required Company from the 'Company Apps' list
 Then Verify 'My Apps' page should contain list of Apps 
 |Apps Message |
 |These are your apps! Explore them!|
 When Click on required App Name from the App list for 'Deletion'
 And Click on 'Delete' button under App Name link
 #Then Application should navigate to Delete App Page by displaying user confirmation as 'Are you sure'
 When Click on 'Delete App' button under 'Delete App' page
 Then App should be Deleted successfully by displaying 'App Deleted' and deleted app should not be exist into the app list
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display 























 