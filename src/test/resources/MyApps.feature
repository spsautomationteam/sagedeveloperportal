@Devportal
Feature: Verify All Features of 'My Apps'
 I want to this template for my feature file
 
 @Validate_AllOptions_Under_MyApps_Page_SignIn
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 			|
 |Username 	 	 |sri555 |
 |Password  	 |midhun@123			|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 Then Verify It should display the required options under 'My Apps' page
 |Fields 				 |
 |My Apps				 | 
 |My Apps (Individual)				   |
 |Company Apps (Team)            |
 |Manage Companies / Invitations |
 |Looks like you don�t have any apps. Get started by adding one.|
 |Add a new App |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
@Validate_AllOptions_Under_AddNewApp_Page_SignIn
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555       |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Add A New App' button 
 Then Verify It should display the required options under 'Add A New App' page
 |Fields 				 |
 |App Name				 | 
 |Details of Integration	 |
 |Integration Security     |
 |Product									 |
 |Create App 							 | 
 |Add App									 |
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 #@Delete_All_Apps_IfExist
 #Scenario: Delete All Apps IfExist 
 #Given Launch Sage Developer portal URL
 #When Click on 'Sign In' tab present at top left
 #Then Should display 'Sign In' screen along with 'Sign In' fields
 #When Enter valid data into User Name or E-mail and Password fields
 #|Fields   		 |Values		 	|
 #|Username 	 	 |sri555 		|
 #|Password  	 |midhun@123	|
 #And Click on 'Sign In' button
 #Then User should login successfully and Logout button should be display
 #When Click on 'My Apps' tab in User Signed screen
 #Then Verify 'My Apps' page should contain list of Apps 
 #|Apps Message |
 #|These are your apps! Explore them!|
 #When If Exist Delete All Creted Apps from the Apps list
 #And Click on 'Logout' tab in 'Sage Developer User' screen
 #Then User should Logout successfully and Sign In tab should be display
 
 @Verify_AllOptions_Keep_Blank_AddNewAppPage_SignIn
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 							|
 |Username 	 	 |sri555 |
 |Password  	 |midhun@123							|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Add A New App' button 
 And Keep All Mandatory fields as Blank under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 | 			| 
 |Details of Integration	 | 			| 
 And Click on 'Create App' button
 Then Verify should display Error msgs for 'Blank Mandatory' fields in Add New App Page
 |App Name field is required.|
 |Machine-readable name field is required.|
 |Details of Integration field is required.|
 |Product field is required.|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display

@Verify_AddNewApp_Functionality_With_ValidData
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 And Click on 'Add A New App' button 
 And Enter Valid data in all fields under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 |ixigo App        | 
 |Details of Integration	 |Contact Details	   |
And Click on 'Edit' link beside the 'App Name' field to view the 'Machine Readable Name' field
Then Verify 'Machine Readable Name' field value should match with 'App Name' value by including iphen
 # As per xpath syntax, should give - symbol before checkbox value(Ex : -Tokens)
 And Select the require drop down item for 'Integration Security'  
 And Select the require Products from the 'Products list'
  |CheckboxName|
	|Bankcard |
	|Bankcard and Token |
	|Merchant Boarding |
	|-PaymentsJS |
	|-Token | 
 And Click on 'Create App' button
 Then Should display 'Success' msg and verify submitted data in 'My Apps' page
 |VerifyValues|
 |My Apps |
 |App Created!|
 |These are your apps! Explore them!|
 When Click on 'App Name' link under 'My Apps' table
 Then Should display list of 'My App' tabs under selected 'App Name' link
 |TabNames 	|
 |Keys	|
 |Products|
 |Details	|
 |Analytics|
 |Edit |
 |Delete|
 And Should display 'App Name' at 'Edit' and 'Delete' buttons
 When Click on 'Products' tab under 'App Name' link
 Then Verify displayng Product Names matching with Selected Products at 'Create App'
 |CheckboxName|
 |Bankcard |
 |Bankcard and Token |
 |Merchant Boarding |
 |PaymentsJS |
 |Token | 
 When Click on 'Details' tab under 'App Name' link
 Then Verify displayng App details matching with 'Create New App' details
 When Click on 'Analytics' tab under 'App Name' link 
 Then Should display list of tabs for selected 'Analytics' tab in 'Analytics' page
 |TabNames 	|
 |Keys	|
 |Products|
 |App Details	|
 |Analytics|
 |Edit App|
 |App Performance|
 When Click on 'Products' tab under 'Analytics' page
 Then Verify displaying Product Names matching with Selected Products at 'Create App'
 When Click on 'App Details' tab under 'Analytics' page
 Then Verify displaying App Details matching with entered details at 'Create App'
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_Edit_Functionality_ForExisting_MyApp
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 Then Verify 'My Apps' page should contain list of Apps 
 |Apps Message |
 |These are your apps! Explore them!|
 When Click on required App Name from the App list for 'Updation'
 And Click on 'Edit' button under App Name link
 Then Application should navigate to 'Edit App' Page
 And Enter Valid data in all fields under 'Add A New App' page
 |Fields 				           | Data |
 |App Name				 				 |Skype App        | 
 |Details of Integration	 |Skype App Details	   |
 #And Select the require checkbox Options for 'Integration Security'
  #|CheckboxName|
#	|Secure Messaging - SEVD/SED |
#	|Response Hash - PaymentsJS |  
 #And Select the require Products from the 'Products list'
  #|CheckboxName|
#	|Bankcard |
#	|Bankcard and Token |
#	|Merchant Boarding |
 And Click on 'Save App' button under 'Edit App' page
 Then Should display 'Success' msg and verify Updated App should listed into the 'My Apps' page
 |VerifyValues|
 |My Apps |
 |App Updated!|
 |These are your apps! Explore them!|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_Delete_Functionality_ForExistingApp
 Scenario: Verify displaying 'controls'at 'My Apps' page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values		 	|
 |Username 	 	 |sri555 		|
 |Password  	 |midhun@123	|
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'My Apps' tab in User Signed screen
 Then Verify 'My Apps' page should contain list of Apps 
 |Apps Message |
 |These are your apps! Explore them!|
 When Click on required App Name from the App list for 'Deletion'
 And Click on 'Delete' button under App Name link
 #Then Application should navigate to Delete App Page by displaying user confirmation as 'Are you sure'
 When Click on 'Delete App' button under 'Delete App' page
 Then App should be Deleted successfully by displaying 'App Deleted' and deleted app should not be exist into the app list
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 
 
 
 
 
 
 
 