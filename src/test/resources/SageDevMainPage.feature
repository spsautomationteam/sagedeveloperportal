@Devportal
Feature: Validate All Tabs and Links in Developer Portal Home Page
 I want to this template for my feature file
 
 @Validate_AllTabsAndLinks_in_DeveloperPortal_HomePage
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'tabApiSandBox' Tab present at top of the Page 
 Then Verify application should navigate to 'API Documentation' page properly
 When Click on 'tabDocumentation' Tab present at top of the Page 
 Then Verify application should navigate to 'Documentation' page properly
 When Click on 'tabForums' Tab present at top of the Page 
 Then Verify application should navigate to 'Forums' page properly
# When Click on 'tabPartners' Tab present at top of the Page 
# Then Verify application should navigate to 'Sage Partners' page properly
 When Click on 'tabSupport' Tab present at top of the Page 
 Then Verify application should navigate to 'Contact Support' page properly
 When Click on 'tabCreateAccount' Tab present at top of the Page 
 Then Verify application should navigate to 'Create Account' page properly
 When Click on 'tabSignIn' Tab present at top of the Page 
 Then Verify application should navigate to 'Sign In' page properly
 When Click on 'linkPaymentsJss' link present at middle of the Page 
 Then Verify application should navigate to 'Payments.js' page properly
 When Click on 'linkSageExchgVirDesk' link present at middle of the Page 
 Then Verify application should navigate to 'Sage Exchange Virtual Desktop v2.0' page properly
 When Click on 'linkSageExchgDesk' link present at middle of the Page 
 Then Verify application should navigate to 'Sage Exchange Desktop v2.0' page properly
 When Click on 'linkDirectAPIs' link present at middle of the Page 
 Then Verify application should navigate to 'Sage Direct API' page properly
 When Click on 'linkAppAPIs' link present at middle of the Page 
 Then Verify application should navigate to 'Sage Application API' page properly
 #       Coming Soon
# When Click on 'Mobile SDKs' link present at middle of the Page 
# Then Verify application should navigate to 'Mobile SDKs' page properly
# When Click on 'Advanced Fraud APIs' link present at middle of the Page 
# Then Verify application should navigate to 'Advanced Fraud APIs' page properly
# When Click on 'Sage Connect' link present at middle of the Page 
# Then Verify application should navigate to 'Sage Connect' page properly 
 #When Click on all Tabs present at middle of the Page 
 #Then Verify corresponding page should display 
 