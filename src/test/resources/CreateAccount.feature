@Devportal
Feature: Verify Create User Account all Features
 I want to this template for my feature file
 
 @Create_Account_All_Controls
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Create Account' tab present at top left
 Then Should display 'Create Account' screen 
 And Should display all require fields to create New User Account
 And Also it should display 3 hyperlinks
 
 @CreateAccount_ValidData_Submit
 Scenario: Verify New User Account Created Successfully with Valid data
 Given Launch Sage Developer portal URL 
 When Click on 'Create Account' tab present at top left
 Then Should display 'Create Account' screen 
 When Enter valid data in all required fields
 |Fields   		 |Values			 |
 |FirstName 	 |San2				 |
 |LastName  	 |Kumar2			 |
 |Username		 |San 				 |
 |EmailAddress |test@gmail.com|
 |CompanyName	 |HP   				 |
 |Country			 |India 			 |
 |Address1		 |Miami 			 |
 |Address2		 |Dallas 			 |
 |City				 |Bangalore 	 |
 |State				 |Karnataka 	 |
 |ZipCode			 |123456 			 | 
 And Click on Create Account button
 Then It should create new account and should display Welcome message along with Sign In button
 
 @Create_Account_With_All_BlankFields
 Scenario: Verify Create Account with All Blank Fields
 Given Launch Sage Developer portal URL 
 When Click on 'Create Account' tab present at top left
 Then Should display 'Create Account' screen 
 When Keep all Mandatory fields are in Blank
 |Fields   		 |Values		|
 |FirstName 	 |					|
 |LastName  	 |					|
 |Username		 | 					|
 |EmailAddress |					|
 |CompanyName	 |  	 			|
 |Address1		 | 					|
 |City				 | 	 				|
 |State				 |- Select -|
 |ZipCode			 | 					|
 And Click on Create Account button
 Then error message body should contain "Username field is required."
 And error message body should contain "First Name field is required."
 And error message body should contain "E-mail address field is required."
 And error message body should contain "Last Name field is required."
 And error message body should contain "Company Name field is required."
 And error message body should contain "Address 1 field is required."
 And error message body should contain "City field is required."
 And error message body should contain "State field is required."
 And error message body should contain "ZIP code field is required."
 And error message body should contain "Accept Terms & Conditions of Use field is required."
 
 