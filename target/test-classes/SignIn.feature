 @Devportal
Feature: Verify user SignIn page
 I want to this template for my feature file
 
 @User_SignIn_With_ValidData_And_LogOut
 Scenario: Verify user able to login into 'Sage Developer Portal' with valid login credentials
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values	  |
 |Username 	 	 |sri555	  |
 |Password  	 |midhun@123  |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @User_SignIn_With_Invalid_Data
 Scenario: Verify user able to login into 'Sage Developer Portal' with valid login credentials
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |kkkkkkk			 |
 |Password  	 |klfkjdffsdf	 |
 And Click on 'Sign In' button
 Then error message body should contain "Sorry, unrecognized username or password. Have you forgotten your password?"
 
 @User_SignIn_With_All_BlankFields
 Scenario: Verify User_SignIn with All Blank Fields
 Given Launch Sage Developer portal URL 
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Keep all SignIn fields are in Blank
 |Fields   		 |Values |
 |Username 	 	 |			 |
 |Password  	 |			 |
 And Click on 'Sign In' button
 Then error message body should contain "User Name or e-mail address field is required."
 And error message body should contain "Password field is required."
