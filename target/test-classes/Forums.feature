@Devportal
Feature: Verify All Features under Forums Page
 I want to this template for my feature file
 
 @Validate_AllTabs_in_Forums_Page_BeforeSignIn
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 Then Verify displaying Tabs at top of the Forums Page2 
 And Verify displaying main Tabs under Forums section2 
 
 @Validate_AllTabs_in_Forums_Page_AfterSignIn
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 Then Verify displaying Tabs at top of the Forums Page 
 And Verify displaying main Tabs under Forums section 
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Validate_AllElements_in_ForumsPage_Under_ViewForumsTab
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 Then Verify It should display below list of the options under 'View Forums' tab 
 |Options |
 |General Discussion|
 |Getting Started|
 |Direct API|
 |PaymentsJS|
 |Sage Exchange Desktop v2.0|
 |Sage Exchange Virtual Desktop v2.0|
 |EMV|
 |New posts|
 |No new posts|
 |- Forum Tools -|
 |View active forum posts|
 |View unanswered forum posts| 
 
 @Select_DropDownItem_ViewActiveForumPosts_In_ViewForumsTab
 Scenario: Select_DropDownItem_ViewActiveForumPosts_In_ViewForumsTab 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 When Select 'View active forum posts' option from Forum Tools drop down
 Then It should switch to 'Active forum topics' tab present in Forums page1
 
 @Select_DropDownItem_Viewunansweredforumposts_In_ViewForumsTab
 Scenario: Verify Select_DropDownItem_Viewunansweredforumposts_In_ViewForumsTab
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 When Select 'View unanswered forum posts' option from Forum Tools drop down
 Then It should switch to 'Unanswered forum topics' tab present in Forums page2
 
 @Collapse_UnCollapse_ForumTable
 Scenario: Verify Collapse_UnCollapse_ForumTable
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 And Verify Height of the Forum Table 'Before' 'Collapsed'
 When Click on collpase '-' icon present at top right corner of the Forums table
 Then Forum Table should be 'Collapsed' properly

@Verify_AllElements_Under_GeneralDiscussion_Forum_WithoutSignIn
 Scenario: Verify_AllElements_Under_GeneralDiscussion_Forum_WithoutSignIn 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab
 Then Verify It should display below list of the options under 'General Discussion' screen 
 |Options |
 |Log in to post new content in the forum.|
 |- Forum Tools -|
 |New posts|
 |No new posts|
 |Hot topic with new posts|
 |Hot topic without new posts|
 |Sticky topic|
 |Locked topic| 
 |Last post|
 |Down|
 |Sort|
 
 @Verify_SortingOf_TopicStarter_ColumnData_Under_GeneralDisc_Table
 Scenario: Verify_SortingOf_TopicStarter_ColumnData_Under_GeneralDisc_Table
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab
 And Select 'Topic / Topic starter' option from drop down which is present below the table
 And Select 'Up' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Data in the 'Topic starter' column should be 'Ascending Order' with xpath 'dataTopicStarter'   
 When Select 'Down' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Data in the 'Topic starter' column should be 'Descending Order' with xpath 'dataTopicStarter' 

@Verify_SortingOf_Replies_ColumnData_Under_GeneralDisc_Table
 Scenario: Verify_SortingOf_Replies_ColumnData_Under_GeneralDisc_Table 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab
 And Select 'Replies' option from drop down which is present below the table
 And Select 'Up' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Data in the 'Replies' column should be 'Ascending Order' with xpath 'dataReplies' 
 When Select 'Down' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Data in the 'Replies' column should be 'Descending Order' with xpath 'dataReplies' 

 @Verify_SortingOf_LastPosts_ColumnData_Under_GeneralDisc_Table
 Scenario: Verify_SortingOf_LastPosts_ColumnData_Under_GeneralDisc_Table
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab
 And Select 'Last post' option from drop down which is present below the table
 And Select 'Up' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Table Data in the 'Last post' column should be 'Ascending Order' with xpath 'dataLastPosts' 
 When Select 'Down' from the next combo selection
 And Click on 'Sort' button beside the drop down
 Then Table Data in the 'Last post' column should be 'Descending Order' with xpath 'dataLastPosts' 
 
 @Verify_AllFields_in_NewTopicsPage_Under_GeneralDiscussion_WithSignIn
 Scenario: Verify_AllFields_in_NewTopicsPage_Under_GeneralDiscussion_WithSignIn 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab  
 And Click on 'NEW TOPIC' link displaying under 'General Discussion' header
 Then Should display 'NEW TOPIC' screen along with require fields 
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_AllFields_With_Blank_In_NewTopics_Page_BySignIn
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab  
 And Click on 'NEW TOPIC' link displaying under 'General Discussion' header 
 Then Should display 'NEW TOPIC' screen along with require fields
 When Keep 'Subject' Mandatory field as Blank and Click on Save button
 Then Error message should be display
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_SaveButton_Functionality_By_EnterData_GeneralDiscussion
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab  
 And Click on 'NEW TOPIC' link displaying under 'General Discussion' header 
 Then Should display 'NEW TOPIC' screen along with require fields
 When Enter valid data into all fields in 'NEW TOPIC' page
 |Fields |Value              |
 |Subject|Testing Subject       |
 |Forums |General Discussion |
 |Body   |Testing Body          | 
 And Click on 'Save' button
 Then The data we entered should be displayed in 'Subject has been Created' page
 |Fields |Value              |
 |Subject|Testing Subject       |
 |Forums |General Discussion |
 |Body   |Testing Body          | 
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 @Verify_PreviewButton_Functionality_By_EnterData_GeneralDiscussion
 Scenario: Verify PreviewButton Functionality By Enter Data GeneralDiscussion 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 And Click on 'View Forums' tab under Forums section
 And Click on 'General Discussion' link under 'View Forums' tab  
 And Click on 'NEW TOPIC' link displaying under 'General Discussion' header 
 Then Should display 'NEW TOPIC' screen along with require fields
 When Enter valid data into all fields in 'NEW TOPIC' page
 |Fields |Value              |
 |Subject|Test Subject       |
 |Forums |General Discussion |
 |Body   |Test Body          | 
 And Click on 'Preview' button
 Then The data we entered should be displayed in Preview Trimmed version Page
 |Fields |Value              |
 |Subject|Test Subject       |
 |Forums |General Discussion |
 |Body   |Test Body          | 
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display

#@Verify_DropDown_GeneralDiscussion_NewTopic_Save
 #Scenario: Verify displaying 'controls'at Create Account page 
 #Given Launch Sage Developer portal URL
 #When Click on 'Sign In' tab present at top left
 #Then Should display 'Sign In' screen along with 'Sign In' fields
 #When Enter valid data into User Name or E-mail and Password fields
 #|Fields   		 |Values			 |
 #|Username 	 	 |sri555			 |
 #|Password  	 |midhun@123	 |
 #And Click on 'Sign In' button
 #Then User should login successfully and Logout button should be display
 #When Click on 'Forums' tab in 'Sage Developer User' screen
 #And Click on 'View Forums' tab under Forums section
 #And Click on 'General Discussion' link under 'View Forums' tab  
 #And Click on 'NEW TOPIC' link displaying under 'General Discussion' header 
 #Then Should display 'NEW TOPIC' screen along with require fields
 #When Enter valid data into all fields in 'NEW TOPIC' page
 #|Fields |Value              |
 #|Subject|Test Subject       |
 #|Forums |General Discussion |
 #|Body   |Test Body          | 
 #And Click on 'Save' button
 #Then The data we entered should be displayed in 'Subject has been Created' page
 #|Fields |Value              |
 #|Subject|Test Subject       |
 #|Forums |General Discussion |
 #|Body   |Test Body          | 
 #When Click on 'General Discussion' Navigation tab at Subject has been created page
 #Then The added Subject text should be listed under 'General Discussion' table  
 #|Fields |Value              |
 #|Subject|Test Subject       |
 #When Click on 'Logout' tab in 'Sage Developer User' screen
 #Then User should Logout successfully and Sign In tab should be display
 
 @Verify_NewTopic_Functionality_For_AllForumTabs
 Scenario: Verify NewTopic Functionality For AllForumTabs
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab in 'Sage Developer User' screen
 And Click on 'View Forums' tab under Forums section
 Then Click on All Forum Tabs and Create New Post and Validate the Submitted data in All required locations    
 |ForumTabs  													|Subject	 		|Body	 		|
 |General Discussion									|GD Subject		|GD Body	|
 |Getting Started											|GS Subject		|GS Body	|
 |Direct API													|DA Subject		|DA Body	|
 |PaymentsJS													|PJ Subject		|PJ Body	|
 |Sage Exchange Desktop v2.0					|SED Subject	|SED Body	|
 |Sage Exchange Virtual Desktop v2.0	|SEVD Subject	|SEVD Body|
 |EMV																	|EMV Subject	|EMV Body	|
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display 
 
 @Verify_AllElements_in_ActiveTopicsTab_Under_ForumsPage
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'Active Topics' tab under Forums section
 Then Verify should display the required options under 'Active Topics' tab 
 
 @Validate_DisplayingData_Under_ActiveTopics_Table_BySelect_AllDropDown_Items
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'Active Topics' tab under Forums section
 #Then Select All list Items and Verify Displaying data under Active Topics table with xpath 'columnForumDataAT'
 Then Select All list Items and Verify Displaying data under 'Active Topics' table with xpath 'columnForumDataActTopic'
 |TopicListItems											|
 |General Discussion									|
 |Getting Started											|
 |Direct API													|
 |PaymentsJS													|
 |Sage Exchange Desktop v2.0					|
 |Sage Exchange Virtual Desktop v2.0	|
 |EMV																	|
  #And Click on Apply button
  #Then Should display list of Topic records for selected list Item under Active Topics table  
 
@Validate_AllElements_in_UnansweredTopics_Tab_Under_Forums
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'Unanswered topics' tab under Forums section
 Then Verify It should display the required options under 'Unanswered topics' tab 
 
 @Validate_DisplayingData_Under_UnansweredTopics_Table_BySelect_AllDropDown_Items
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Forums' tab present at top left
 And Click on 'Unanswered topics' tab under Forums section
 Then Select All list Items and Verify Displaying data under 'Unanswered Topics' table with xpath 'columnForumDataUnAnswrd'
 |TopicListItems											|
 |General Discussion									|
 |Getting Started											|
 |Direct API													|
 |PaymentsJS													|
 |Sage Exchange Desktop v2.0					|
 |Sage Exchange Virtual Desktop v2.0	|
 |EMV																	|
  #And Click on Apply button
  #Then Should display list of Topic records for selected list Item under Active Topics table  
 
 @Validate_AllElements_in_NewUpdatedTopics_Tab_Under_Forums
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab present at top left
 And Click on 'New & updated topics' tab under Forums section
 Then Verify It should display the required options under 'New & updated topics' tab
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display 
 
 @Validate_DisplayingData_Under_NewUpdatedTopics_Table_BySelect_AllDropDown_Items
 Scenario: Verify displaying 'controls'at Create Account page 
 Given Launch Sage Developer portal URL
 When Click on 'Sign In' tab present at top left
 Then Should display 'Sign In' screen along with 'Sign In' fields
 When Enter valid data into User Name or E-mail and Password fields
 |Fields   		 |Values			 |
 |Username 	 	 |sri555			 |
 |Password  	 |midhun@123	 |
 And Click on 'Sign In' button
 Then User should login successfully and Logout button should be display
 When Click on 'Forums' tab present at top left
 And Click on 'New & updated topics' tab under Forums section
 Then Select All list Items and Verify Displaying data under 'New & updated topics' table with xpath 'columnForumDataNewUpdate'
 |TopicListItems											|
 |General Discussion									|
 |Getting Started											|
 |Direct API													|
 |PaymentsJS													|
 |Sage Exchange Desktop v2.0					|
 |Sage Exchange Virtual Desktop v2.0	|
 |EMV																	| 
  #And Click on Apply button
  #Then Should display list of Topic records for selected list Item under Active Topics table
 When Click on 'Logout' tab in 'Sage Developer User' screen
 Then User should Logout successfully and Sign In tab should be display
 
 
 
 