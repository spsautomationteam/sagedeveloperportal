$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/SageDevMainPage.feature");
formatter.feature({
  "line": 2,
  "name": "Validate All Tabs and Links in Developer Portal Home Page",
  "description": "I want to this template for my feature file",
  "id": "validate-all-tabs-and-links-in-developer-portal-home-page",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@Devportal"
    }
  ]
});
formatter.before({
  "duration": 11718476200,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Verify displaying \u0027controls\u0027at Create Account page",
  "description": "",
  "id": "validate-all-tabs-and-links-in-developer-portal-home-page;verify-displaying-\u0027controls\u0027at-create-account-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@Validate_AllTabsAndLinks_in_DeveloperPortal_HomePage"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Launch Sage Developer portal URL",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Click on \u0027tabApiSandBox\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "Verify application should navigate to \u0027API Documentation\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Click on \u0027tabDocumentation\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "Verify application should navigate to \u0027Documentation\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "Click on \u0027tabForums\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "Verify application should navigate to \u0027Forums\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "comments": [
    {
      "line": 14,
      "value": "# When Click on \u0027tabPartners\u0027 Tab present at top of the Page"
    },
    {
      "line": 15,
      "value": "# Then Verify application should navigate to \u0027Sage Partners\u0027 page properly"
    }
  ],
  "line": 16,
  "name": "Click on \u0027tabSupport\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "Verify application should navigate to \u0027Contact Support\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Click on \u0027tabCreateAccount\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "Verify application should navigate to \u0027Create Account\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "Click on \u0027tabSignIn\u0027 Tab present at top of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "Verify application should navigate to \u0027Sign In\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Click on \u0027linkPaymentsJss\u0027 link present at middle of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "Verify application should navigate to \u0027Payments.js\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Click on \u0027linkSageExchgVirDesk\u0027 link present at middle of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "Verify application should navigate to \u0027Sage Exchange Virtual Desktop v2.0\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "Click on \u0027linkSageExchgDesk\u0027 link present at middle of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "Verify application should navigate to \u0027Sage Exchange Desktop v2.0\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "Click on \u0027linkDirectAPIs\u0027 link present at middle of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 29,
  "name": "Verify application should navigate to \u0027Sage Direct API\u0027 page properly",
  "keyword": "Then "
});
formatter.step({
  "line": 30,
  "name": "Click on \u0027linkAppAPIs\u0027 link present at middle of the Page",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "Verify application should navigate to \u0027Sage Application API\u0027 page properly",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateAccount.developer_Portal_is_opened()"
});
formatter.result({
  "duration": 1277395600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabApiSandBox",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 5949449699,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "API Documentation",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 5080544400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabDocumentation",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 5477017501,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Documentation",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4394015500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabForums",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 5410171799,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Forums",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4717090000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabSupport",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 5449849801,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Contact Support",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 5112282499,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabCreateAccount",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 5467033800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Create Account",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4674942500,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "tabSignIn",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Tabs_Top_Of_Page(String)"
});
formatter.result({
  "duration": 4805439800,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sign In",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 5297422100,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "linkPaymentsJss",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Links_Middle_Of_Page(String)"
});
formatter.result({
  "duration": 5649518300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Payments.js",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4842448301,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "linkSageExchgVirDesk",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Links_Middle_Of_Page(String)"
});
formatter.result({
  "duration": 5555494499,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage Exchange Virtual Desktop v2.0",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4435974300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "linkSageExchgDesk",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Links_Middle_Of_Page(String)"
});
formatter.result({
  "duration": 5052388199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage Exchange Desktop v2.0",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4865011400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "linkDirectAPIs",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Links_Middle_Of_Page(String)"
});
formatter.result({
  "duration": 5014617200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage Direct API",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4750543700,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "linkAppAPIs",
      "offset": 10
    }
  ],
  "location": "SageDevMainPage.click_on_All_Links_Middle_Of_Page(String)"
});
formatter.result({
  "duration": 5307444301,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sage Application API",
      "offset": 39
    }
  ],
  "location": "SageDevMainPage.Verify_Application_should_navigateTo_Corresponding_Page(String)"
});
formatter.result({
  "duration": 4686958600,
  "status": "passed"
});
formatter.after({
  "duration": 877694899,
  "status": "passed"
});
});